<!DOCTYPE html>
<html>
<head>
<title>@yield('title') | Surya Bali Art Instrumental Music</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
	function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{ asset('client/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('client/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('client/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{ asset('client/css/animate.css') }}">
<!-- font-awesome icons -->
<link href="{{ asset('css/icons/fontawesome/css/all.min.css') }}" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="{{ asset('client/js/jquery-1.11.1.min.js') }}"></script>
<!-- //js -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{ asset('client/js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('client/js/easing.js')}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
@yield('styles')
</head>
	
<body>
    @include('client.cart.modal')
    @include('client.header', ['authenticated' => Auth::guard('pelanggan')->check(), 'user' => Auth::guard('pelanggan')->user()])
    
	@yield('content')
    
    @include('client.footer')

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('client/js/bootstrap.min.js')}}"></script>

    <!-- top-header and slider -->
    <!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
    <!-- //here ends scrolling icon -->
    <!-- main slider-banner -->
    <script src="{{ asset('client/js/skdslider.min.js') }}"></script>
    <link href="{{ asset('client/css/skdslider.css') }}" rel="stylesheet">
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
                        
            jQuery('#responsive').change(function(){
            $('#responsive_wrapper').width(jQuery(this).val());
            });
            
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#your-account').click(function(){
                $('.user-profile').toggle();
            });
        });
    </script>
    <!-- //main slider-banner --> 

    @if(session()->has('ok'))
    <script>
        alert("<?= session()->get('ok') ?>");
    </script>
    @endif
    @if(session()->has('fail'))
    <script>
        alert("<?= session()->get('fail') ?>");
    </script>
    @endif
    <script src="{{ asset('client/js/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('client/js/custom.js') }}"></script>
    @stack('scripts')
</body>
</html>