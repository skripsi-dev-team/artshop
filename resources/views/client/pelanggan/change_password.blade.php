@extends('client.app')

@section('title')
Edit Profile
@endsection

@section('content')

<div class="container">
    <div class="profile-page">
        
        <div class="row">
            <div class="col-md-6">
                @if($errors->any())
                    <div class="alert alert-danger client-error">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger client-error">
                        <?= session()->get('error') ?>
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success client-error">
                        <?= session()->get('success') ?>
                    </div>
                @endif
                
                <form action="{{ route('pelanggan.update_password', ['id' => $profile->id_pelanggan]) }}" method="post">
                @csrf 
                @method('PUT')
                    <div class="form-group">
                        <label for="old_password">Password Lama</label>
                        <input type="password" class="form-control" name="old_password">
                    </div>
                    <div class="form-group">
                        <label for="password">Password Baru</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <div class="form-group">
                        <label for="password">Re-type Password</label>
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                   
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection