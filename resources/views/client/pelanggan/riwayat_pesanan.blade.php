@extends('client.app')

@section('title')
Riwayat Pesanan
@endsection

@section('content')

@include('client.breadcrumb')


<div class="checkout">
    <div class="container">
        <h2>Riwayat Pesanan Anda: </h2>
        <div class="checkout-right">
            <table class="timetable_sub">
                <thead>
                    <tr>
                        <th>Kode Pemesanan</th>	
                        <th>Tanggal</th>
                        <th>Total Nominal</th>
                        <th>Status Bayar</th>
                        <th>Status Pemesanan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($pemesanan as $data)
                        <tr class="rem1">
                            <td class="invert">{{ $data->kode_pemesanan }}</td>
                            <td class="invert">{{ $data->tanggal }}</td>
                            <td class="invert">Rp. {{ number_format($data->total_nominal) }}</td>
                            <td class="invert">{{ $data->status_bayar == 0 ? 'Belum Dibayar' : 'Sudah Dibayar' }}</td>
                            <td class="invert">{{ $data->getStatusText() }}</td>
                            <td class="invert">
                                <a href="{{ route('pelanggan.detail_pesanan', ['kode_pemesanan' => $data->kode_pemesanan]) }}" class="btn btn-primary btn-sm">Detail</a>
                                @if($data->status_bayar != 0)
                                    <a href="#" class="btn btn-success btn-sm" disabled>Sudah Dibayar</a>
                                    <a href="#" class="btn btn-danger btn-sm " disabled>Batalkan Pesanan</a>
                                    @elseif($data->status_pemesanan == 5)
                                            <a href="#" class="btn btn-success btn-sm" disabled>Bayar Sekarang</a>
                                            <a href="#" class="btn btn-danger btn-sm " disabled>Batalkan Pesanan</a>
                                    @else
                                    <form action="https://staging.doku.com/Suite/Receive" method="POST">
                                        <input type="hidden" name="MALLID" value="{{ env('MALLID', null) }}">
                                        <input type="hidden" name="CHAINMERCHANT" value="NA">
                                        <input type="hidden" name="SESSIONID" value="{{ $data->id_pemesanan }}">
                                        <input type="hidden" name="AMOUNT" value="{{ $data->total_nominal.'.00' }}">
                                        <input type="hidden" name="PURCHASEAMOUNT" value="{{ $data->total_nominal.'.00' }}">
                                        <input type="hidden" name="TRANSIDMERCHANT" value="{{ $data->kode_pemesanan }}">
                                        <input type="hidden" name="WORDS" value="{{ getWordsKey($data->total_nominal, $data->kode_pemesanan) }}">
                                        <input type="hidden" name="REQUESTDATETIME" value="{{ now()->format('YmdHis') }}">
                                        <input type="hidden" name="CURRENCY" value="360">
                                        <input type="hidden" name="PURCHASECURRENCY" value="360">
                                        <input type="hidden" name="BASKET" value="{{ createBasket($data) }}">
                                        <input type="hidden" name="NAME" value="{{ $data->customer->name }}">
                                        <input type="hidden" name="EMAIL" value="{{ $data->customer->email }}">
                                        <input type="submit" value="Bayar Sekarang" class="btn btn-success btn-sm">
                                    </form>
                                    <form action="{{ route('pelanggan.cancel_order', ['id' => $data->id_pemesanan]) }}" method="post">
                                        @csrf 
                                        @method('put')
                                        <input type="submit" class="btn btn-danger btn-sm" value="Batalkan Pesanan" onclick="return confirm('Yakin akan membatalkan pesanan?');">
                                    </form>
                                    
                                @endif
                            </td>
                        </tr>  
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada pemesanan</td>
                        </tr>

                        @endforelse
                   
                </tbody>
            </table>
        </div>
        
    </div>
</div>

@endsection