@extends('client.app')

@section('title')
Edit Profile
@endsection

@section('content')

<div class="container">
    <div class="profile-page">
        
        <div class="row">
            <div class="col-md-6">
                @if($errors->any())
                    <div class="alert alert-danger client-error">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                
                <form action="{{ route('pelanggan.update', ['id' => $profile->id_pelanggan]) }}" method="post">
                @csrf 
                @method('PUT')
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input type="text" class="form-control" name="name" value="{{ $profile->name }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ $profile->email }}">
                    </div>
                    <div class="form-group">
                        <label for="no_telp">No Telp</label>
                        <input type="number" class="form-control" name="no_telp" value="{{ $profile->no_telp }}">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" class="form-control">{{ $profile->alamat }}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection