@extends('client.app')

@section('title')
Profile
@endsection

@section('content')
<div class="container">
    <div class="profile-page">
        <div class="row">
            <div class="col-md-6">
                <h2>{{ $profile->name }}</h2>
                <table class="table">
                    <tr>
                        <td> Email </td>
                        <td>: </td>
                        <td>{{ $profile->email }}</td>
                    </tr>
                    <tr>
                        <td> Alamat </td>
                        <td>: </td>
                        <td>{{ $profile->alamat }}</td>
                    </tr>
                    <tr>
                        <td> Telp </td>
                        <td>: </td>
                        <td>{{ $profile->no_telp }}</td>
                    </tr>
                </table>
                <a href="{{ route('pelanggan.edit-profile', ['id' => $profile->id_pelanggan]) }}">Edit Profile</a>
            </div>
        </div>
    </div>
</div>

@if(session()->has('success_update'))
   <script>
    alert("<?= session()->get('success_update') ?>");
   </script>
@endif
@endsection