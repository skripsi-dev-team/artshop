@extends('client.app')

@section('title')
Detail Pesanan {{ $pemesanan->kode_pesanan }}
@endsection

@section('content')
@include('client.breadcrumb')
<div class="checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            @if (session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>{{ session()->get('success') }}</strong>
                </div>
            @endif
            </div>
        </div>
        <div class="row" style="margin-top:0">	
            <div class="col-md-6">
                <table class="table">
                    <tr class="bg-primary">
                        <th colspan="2"><h4>Detail Pesanan</h4></th>                        
                    </tr>
                    <tr>
                        <td>Kode Pemesanan </td>
                        <td>{{ $pemesanan->kode_pemesanan }}</td>
                    </tr>
                    <tr>
                        <td>Status </td>
                        <td>{!! $pemesanan->statusBadge() !!}</td>
                    </tr>
                    <tr>
                        <td>Nama Pelanggan </td>
                        <td>{{ $pemesanan->customer->name }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal </td>
                        <td>{{ $pemesanan->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>Total </td>
                        <td>{{ number_format($pemesanan->total_nominal) }}</td>
                    </tr>

                </table>

               
                <br>
                @if($pemesanan->status_bayar != 0)
                    <a href="#" class="btn btn-success btn-lg" disabled>Sudah Dibayar</a>
                @elseif($pemesanan->status_pemesanan == 5)
                    <a href="#" class="btn btn-success btn-lg" disabled>Pesanan dibatalkan</a>
                @else
                    <form action="https://staging.doku.com/Suite/Receive" method="POST">
                        <input type="hidden" name="MALLID" value="{{ env('MALLID', null) }}">
                        <input type="hidden" name="CHAINMERCHANT" value="NA">
                        <input type="hidden" name="SESSIONID" value="{{ $pemesanan->id_pemesanan }}">
                        <input type="hidden" name="AMOUNT" value="{{ $pemesanan->total_nominal.'.00' }}">
                        <input type="hidden" name="PURCHASEAMOUNT" value="{{ $pemesanan->total_nominal.'.00' }}">
                        <input type="hidden" name="TRANSIDMERCHANT" value="{{ $pemesanan->kode_pemesanan }}">
                        <input type="hidden" name="WORDS" value="{{ getWordsKey($pemesanan->total_nominal, $pemesanan->kode_pemesanan) }}">
                        <input type="hidden" name="REQUESTDATETIME" value="{{ now()->format('YmdHis') }}">
                        <input type="hidden" name="CURRENCY" value="360">
                        <input type="hidden" name="PURCHASECURRENCY" value="360">
                        <input type="hidden" name="BASKET" value="{{ createBasket($pemesanan) }}">
                        <input type="hidden" name="NAME" value="{{ $pemesanan->customer->name }}">
                        <input type="hidden" name="EMAIL" value="{{ $pemesanan->customer->email }}">
                        <input type="submit" value="Bayar Sekarang" class="btn btn-success btn-lg">
                    </form>
                @endif
            </div>
            
            <div class="col-md-6">
                <h3>Detail Pembayaran</h3>
                <br>
                @if($pemesanan->payment != null)
                <table class="table">
                    <tr>
                        <td>No. Invoice</td>
                        <td>:</td>
                        <td>{{ $pemesanan->payment->no_pembayaran }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ $pemesanan->payment->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>Metode Pembayaran</td>
                        <td>:</td>
                        <td>{{ $pemesanan->payment->metode_pembayaran }}</td>
                    </tr>
                    <tr>
                        <td>Nominal Pembayaran</td>
                        <td>:</td>
                        <td>{{ number_format($pemesanan->payment->total_bayar) }}</td>
                    </tr>
                </table>
                
                <form action="{{ route('pelanggan.terima_barang', ['id' => $pemesanan->id_pemesanan]) }}" method="post">
                    @method('put')
                    @csrf
                    <input type="hidden" name="status_pemesanan" value="4">
                    
                    <input type="submit" value="Terima Barang" class="btn btn-primary {{ ($pemesanan->status_pemesanan == 4) ? 'disabled' : '' }}">
                </form>

                @else
                <p>Belum ada pembayaran</p>
                @endif
            </div>

           
            <div class="clearfix"> </div>
        </div>
       
        <br>
        
        <br>
        <div class="checkout-right">
            <table class="timetable_sub">
                <thead>
                    <tr>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Total</th>	
                        <th>Foto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail_pemesanan as $data)
                        <tr class="rem1">
                            <td class="invert">{{ $data->barang->nama_barang }}</td>
                            
                            <td class="invert">Rp. {{ ($data->barang->diskon == 0 || $data->barang->diskon == null) ? number_format($data->barang->harga) : number_format($data->barang->getDiscountPrice()) }}</td>
                            <td class="invert">{{ $data->qty }}</td>
                            <td class="invert">Rp. {{ ($data->barang->diskon == 0 || $data->barang->diskon == null) ? number_format($data->barang->harga * $data->qty) : number_format($data->barang->getDiscountPrice() * $data->qty)  }}</td>
                            <td class="invert-image"><a href="{{ route('client.detail_barang', ['slug' => $data->barang->slug ]) }}"><img src="<?= asset('storage/gambar_produk/small').'/'.$data->barang->foto ?>" alt=" " class="img-responsive"></a></td>
                            
                        </tr>  
                    @endforeach
                        <tr>
                            <td >Ongkir</td>
                            <td colspan="4" style="text-align:left">{{ $pemesanan->shipping->ongkos_kirim }}</td>
                        </tr>
                   
                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
