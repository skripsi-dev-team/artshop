    <!-- //footer -->
    <div class="footer" id="contact">
		<div class="container">
			<div class="w3_footer_grids">
				<div class="col-md-4 w3_footer_grid">
					<h3>Contact</h3>
					
					<ul class="address">
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Jl. Patih Jelatik No. 444, Legian <span> Badung, Bali, Indonesia.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">suryabaliart@gmail.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>081916401820</li>
					</ul>
				</div>
				<div class="col-md-4 w3_footer_grid">
					<h3>Informasi</h3>
					<ul class="info"> 
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="<?= url('about') ?>">Tentang</a></li>
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="<?= url('barang') ?>">Produk</a></li>
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#contact">Kontak</a></li>
					</ul>
				</div>
				<div class="col-md-4 w3_footer_grid">
					<h3>Kategori</h3>
					<ul class="info">
						@foreach(listFooterCategory() as $footer_category)
							<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{ route('client.kategori', ['id' => $footer_category->id_kategori]) }}">{{ ucwords($footer_category->nama_kategori) }}</a></li>
						@endforeach 						
					</ul>
				</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
		
		<div class="footer-copy">
			
			<div class="container">
				<p>© 2019 Surya Bali ART. All rights reserved </p>
			</div>
		</div>
		
	</div>	
	
    <!-- //footer -->