<!-- breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            
            <li><a href="{{ route('client.home') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
            <!-- <li class="active">Products</li> -->
            <?= generateBreadcrumb() ?>
        </ol>
    </div>
</div>
<!-- //breadcrumbs -->