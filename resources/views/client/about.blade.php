@extends('client.app')

@section('title')
Tentang Kami
@endsection

@section('content')
	

	<!-- HOME TENTANG KAMI -->
	<div class="container home-about top-brands">
		<div class="row">
			<div class="col-md-12">
				<h2>Tentang Kami</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">				
				<p>“Who We Are ?</p>
				<p>Surya Bali Art adalah sebuah toko yang menjual alat musik handmade yang mengutamakan kualitas dan kepuasan pelanggan. Toko ini terletak di Jalan Raya Patih Jelatik No. 444 Legian, dengan posisi yang strategis membuat Surya Bali Art memiliki peluang bisnis yang menjanjikan, selain muncul dalam bentuk toko fisik, Surya Bali Art juga muncul dalam bentuk toko online untuk menjangkau konsumen yang menginginkan alat musik dengan harga yang bersaing dan kualitas yang terbaik.”</p>

			</div>
		</div>
	</div>
	


	<!-- OUR SERVICE -->
	<div class="container home-service top-brands">
		<div class="row">
			<div class="col-md-12">
				<h2>Layanan Kami</h2>
			</div>
		</div>
		<div class="row service-content">
			<div class="col-md-4">
				<h4>Harga Termurah</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
			<div class="col-md-4">
				<h4>Produk Original</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
			<div class="col-md-4">
				<h4>Bergaransi</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
		</div>
	</div>

 	
	
    
@endsection