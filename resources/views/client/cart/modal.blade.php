<div id="cartModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Your cart</h4>
        </div>
        <div class="modal-body">
          <table class="table">
              <thead>
                  <th>&nbsp;</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                  <th>Qty</th>
              </thead>
              <tbody class="cart-items">
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="3"><b>Total:</b> </td>
                  <td id="cartTotal"></td>
                </tr>
              </tfoot>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary update-cart">Update Cart</button>
          <a href="{{ route('client.checkout') }}" class="btn btn-success">Checkout <i class="fa fa-shopping-cart"></i></a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->