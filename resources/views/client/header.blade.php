    <!-- header -->
	@if(session()->has('notif_login'))
	<script>
		alert("<?= session()->get('notif_login') ?>");
	</script>
	@endif
    <div class="agileits_header">
		<div class="container header-cont">
			<div class="w3l_offers">
				<p>Kepuasan Pelanggan Adalah Keutamaan Kami. <a href="{{ route('client.show_barang') }}">BELANJA SEKARANG</a></p>
			</div>
			<div class="product_list_header">  
				<button class="w3view-cart" type="button" value="" data-toggle="modal" data-target="#cartModal"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
			</div>
        @if($authenticated)
		<div class="logged-in">
			<a href="#" id="your-account">{{ $user->name }}</a>
			<div class="user-profile"> 
				<ul>
					<li><a href="{{ route('pelanggan.profile', ['id' => $user->id_pelanggan ]) }}">{{ $user->email }}</a></li>
					<li><a href="{{ route('pelanggan.liked-items') }}">Barang yang saya suka</a></li>
					<li><a href="{{ route('pelanggan.riwayat_pesanan') }}">Riwayat Pesanan</a></li>
					<li><a href="{{ route('pelanggan.change_password', ['id' => $user->id_pelanggan]) }}">Ganti Password</a></li>
					<li>
						<form action="{{ route('pelanggan.logout') }}" method='post'>
							@csrf
							<button type="submit" class="btn btn-logout">Logout</button>
						</form>
						
					</li>
				</ul>
			</div>
		</div>
    	@else
        <div class="agile-login">
            <ul>
                <li><a href="{{ route('pelanggan.register') }}"> Buat Akun </a></li>
                <li><a href="{{ route('pelanggan.loginForm') }}">Login </a></li>
            </ul>
        </div>
		
		@endif
		<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Telpon Sekarang : 081 916 401 820</li>
					
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="{{ route('client.home') }}">Surya Bali Art</a></h1>
			</div>
		<div class="w3l_search">
			<form method="get" action="{{ route('barang.search') }}">
				<input type="search" name="search" placeholder="Search for a Product..." name="search" value="{{ request()->has('search') ? request()->get('search') : '' }}">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
				<div class="clearfix"></div>
			</form>
		</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>
    <!-- //header -->
    <!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div> 
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<li class="active"><a href="{{ route('client.home') }}" class="act">Home</a></li>	
						<li class="active"><a href="{{ url('about') }}" class="act">About</a></li>
						<li class="active"><a href="{{ route('client.show_barang') }}" class="act">Produk</a></li>	
						<!-- Mega Menu -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Kategori<b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="row">
									<div class="multi-gd-img">
										<ul class="multi-column-dropdown">
											<h6>Kategori</h6>
											@foreach(listCategory() as $list)
												<li><a href="{{ route('client.kategori', ['id' => $list->id_kategori]) }}">{{ ucwords($list->nama_kategori) }}</a></li>
											@endforeach
											
										</ul>
									</div>	
									
								</div>
							</ul>
						</li>
					
						<li><a href="#contact">Contact</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<!-- //navigation -->
