@extends('client.app')

@section('title')
Produk
@endsection

@section('content')

@include('client.breadcrumb')
@if(session()->has('barang_error'))
   <script>
    alert("<?= session()->get('barang_error') ?>");
   </script>
@endif
    <!--- products --->
    <div class="products">
		<div class="container">
			<div class="col-md-4 products-left">
				@include('client.sidebar_produk')																																											
			</div>
			<div class="col-md-8 products-right">
				
				@if(count($barang) != 0)
				<div class="agile_top_brands_grids">
					@if(isset($category))
					<h4 class="category-page">{{ $category }}</h4>
					<hr>
					@endif
                    @foreach($barang as $data)
                        <div class="col-md-4 top_brand_left">
                    
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid">
                                    <div class="agile_top_brand_left_grid_pos">
                                        @if($data->diskon != 0 || $data->diskon != null)
                                            <span class="discount-badge">Discount {{ $data->diskon }}%</span>
                                        @endif
                                    </div>
                                    <div class="agile_top_brand_left_grid1">
                                        <figure>
                                            <div class="snipcart-item block" >
                                                <div class="snipcart-thumb">
                                                    <a href="{{ route('client.detail_barang', ['slug' => $data->slug ]) }}"><img alt="{{ $data->foto }}" src="{{ $data->getImageUrl() }}" class="img-responsive" /></a>		
                                                    <p>{{ $data->nama_barang }}</p>																
                                                    <h4>
                                                        @if($data->diskon == 0 || $data->diskon == null)
                                                            Rp. {{ number_format($data->harga) }}
                                                        @else
                                                            Rp. {{ number_format($data->getDiscountPrice()) }}
                                                            <span>Rp. {{ number_format($data->harga) }}</span>
                                                        @endif																 
                                                    </h4>
                                                </div>
                                                <div class="snipcart-details top_brand_home_details">
                                                    <input type="button" value="Add to cart" data-id="{{ $data->id_barang }}" data-url="{{ route('api.cart.add') }}" class="button add-to-cart">
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
              
                    
					<div class="clearfix"> </div>
				</div>
				@else
					<p>Barang tidak ada</p>
				@endif
				
				<div class="numbering">
                    @if ($barang instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        {{ $barang->links() }}
                    @endif
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
    <!--- products --->

@endsection

