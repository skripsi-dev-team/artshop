@extends('client.app')

@section('title')
{{ $barang->nama_barang }}
@endsection

@section('content')

@include('client.breadcrumb')
<div class="products">
    <div class="container">
        <div class="agileinfo_single">
            <div class="col-md-4 agileinfo_single_left">
                <img id="example" src="<?= asset('storage/gambar_produk').'/'.$barang->foto ?>" alt=" {{ $barang->foto }} " class="img-responsive">
            </div>
            <div class="col-md-8 agileinfo_single_right">
                <h2>{{ $barang->nama_barang }}</h2>
                <span class="category">Kategori: <a href="{{ route('client.kategori', ['kategori_id' => $barang->kategori_id ]) }}">{{ $barang->category->nama_kategori }} </a></span>
                
                @if($barang->diskon != 0 || $barang->diskon != null)
                    <div class="detail-diskon-badge">Discount {{ $barang->diskon }}%</div>
                @endif
                <div class="snipcart-item block">
                    <div class="snipcart-thumb agileinfo_single_right_snipcart detail-price">
                        <h4>
                            @if($barang->diskon == 0 || $barang->diskon == null)
                                Rp. {{ number_format($barang->harga) }}
                            @else
                                Rp. {{ number_format($barang->getDiscountPrice()) }}
                                <span>Rp. {{ number_format($barang->harga) }}</span>
                            @endif																 
                        </h4>
                    </div>
                    <div class="snipcart-details agileinfo_single_right_details">
                        @if ($authenticated)
                            <div class="like-el">
                                <p>Sukai barang ini</p>
                                <div class="like-icon"><a href="#" title="Like"></a><i id="like" data-url="{{ route('api.like.add', $barang->id_barang) }}" class="{{ $barang->inLikeList($user) ? 'fa fa-thumbs-up liked':'far fa-thumbs-up' }}"></i></a></div>
                            </div>
                        @endif
                        <input type="button" value="Add to cart" class="button add-to-cart" data-url="{{ route('api.cart.add') }}" data-id="{{ $barang->id_barang }}">                        
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <br>
   
    <!-- diskripsi & comment -->
    <div class="container">
        <div class="grid_3 grid_5">
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">Keterangan</a></li>
                    <li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Komentar</a></li>
                </ul>
                <div id="myTabContent" class="tab-content tab-barang">
                    <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
                        <div class="agile-tp">
                            <h5>Deskripsi Barang:</h5>
                             {!! $barang->deskripsi !!}
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
                        @if($barang->comments()->count() != 0)
                            @foreach($barang->comments as $comment)
                                <div class="agile-tp">
                                    <h5>{{ $comment->pelanggan->email }}</h5>
                                    <p class="w3l-ad">{{ $comment->komen }}</p>
                                </div>
                                <hr>
                            @endforeach
                        @else
                        <strong>Belum ada komentar</strong>
                        
                        <hr>
                        @endif
                        
                        @if($authenticated)
                        <div class="div agile-tp">
                            <p>Tambah Komentar:</p>
                            <form action="{{ route('pelanggan.komen', ['slug' => $barang->slug]) }}" method="post">
                            @csrf
                                <textarea name="komen" class="form-control" rows="5" placeholder="Masukan komentar anda disini.."></textarea>
                                <br>
                                <input type="submit" class="btn btn-lg" value="Comment">
                            </form>
                        </div>
                        @else
                        <div class="div agile-tp">
                            <a href="#" data-toggle="modal" data-target="#loginModal">Login</a> untuk menambahkan komentar
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@if(count($similar) != 0)
    <div class="newproducts-w3agile">
		<div class="container">
			<h3>Produk Serupa</h3>
				<div class="agile_top_brands_grids">
                    @foreach($similar as $row)
                        <div class="col-md-3 top_brand_left">
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid">
                                    <div class="agile_top_brand_left_grid_pos">
                                        @if($row->diskon != 0 || $row->diskon != null)
                                            <span class="discount-badge">Discount {{ $row->diskon }}%</span>
                                        @endif
                                    </div>
                                    <div class="agile_top_brand_left_grid1">
                                        <figure>
                                            <div class="snipcart-item block" >
                                                <div class="snipcart-thumb">
                                                    <a href="{{ route('client.detail_barang', ['slug' => $row->slug ]) }}"><img alt="{{ $row->foto }}" src="{{ $row->getImageUrl() }}" class="img-responsive" /></a>		
                                                    <p>{{ $row->nama_barang }}</p>																
                                                    <h4>
                                                        @if($row->diskon == 0 || $row->diskon == null)
                                                            Rp. {{ number_format($row->harga) }}
                                                        @else
                                                            Rp. {{ number_format($row->getDiscountPrice()) }}
                                                            <span>Rp. {{ number_format($row->harga) }}</span>
                                                        @endif																 
                                                    </h4>
                                                </div>
                                                <div class="snipcart-details top_brand_home_details">
                                                    <input type="button" value="Add to cart" data-id="{{ $row->id_barang }}" data-url="{{ route('api.cart.add') }}" class="button add-to-cart">
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
						<div class="clearfix"> </div>
				</div>
		</div>
	</div>
@endif

@endsection