@extends('client.app')

@section('title')
Login Pelanggan
@endsection

@section('content')
<div class="container">
    
    <div class="row login-page">
        <h3>Silahkan Login</h3>
        <form method="POST" action="{{ route('pelanggan.login') }}">
            @csrf
            @if($errors->any())
                <div class="col-md-12">
                    <div class="alert alert-danger client-error">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <input type="submit" class="btn btn-warning" value="Simpan">
                <a href="{{ route('pelanggan.registerForm') }}" class="register-link">Belum punya akun? Daftar disini</a>
            </div> 
        </form>
    </div>
</div>

@endsection