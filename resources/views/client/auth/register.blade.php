@extends('client.app')

@section('title')
Register Pelanggan
@endsection

@section('content')
<div class="container register-page">
    <h2>Register Pelanggan</h2>
    <hr>
    <form method="POST" action="{{ route('pelanggan.register') }}" class="register-form">
        @csrf
        @if($errors->any())
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger client-error">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                        
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_pelanggan">Nama Pelanggan</label>
                    <input type="text" class="form-control" name="name">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="password">Retype Password</label>
                    <input type="password" class="form-control" name="password_confirmation">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea class="form-control" name="alamat"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="no_telp">No Telp</label>
                    <input type="text" class="form-control" name="no_telp">
                </div>
                
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-warning" value="Daftar">
            </div>
            
        </div>
    </form>
</div>

@if(session()->has('success_register'))
   <script>
    alert("<?= session()->get('success_register') ?>");
   </script>
@endif

@endsection

