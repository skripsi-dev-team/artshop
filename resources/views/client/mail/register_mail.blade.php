<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
body {
    background:#F1F1F1;
}

.container {
    background:white;
    border-radius:4px;
    margin-top:50px;
    margin-bottom:50px;
}

.header {
    padding-top:30px;
    padding-bottom:30px;
}

table {
    background: #fafafa;
}
</style>
<body>
    <div class="container">
        <div class="row header">
            <div class="col-md-12">
                <h2>Registrasi Berhasil</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <p>
                Dear <b> {{ $pelangganName }}, </b>
            </p>
            <p>Terima kasih telah melakukan pendaftaran pada website kami, sekarang anda dapat melakukan pembelian produk, like, komentar dan lain-lain</p>
            <p>Berikut adalah data yang terdaftar:</p>
            <table class="table">
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ $pelangganName }}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>{{ $pelangganEmail }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{ $pelangganAlamat }}</td>
                </tr>
                <tr>
                    <td>No Telp</td>
                    <td>:</td>
                    <td>{{ $pelangganNoTelp }}</td>
                </tr>
            </table>
            </div>
        </div>
        <div class="row header">
            <div class="col-md-12">
                <p>Selamat bergabung dan selamat berbelanja :)</p>
            </div>
        </div>
    </div>
    
</body>
