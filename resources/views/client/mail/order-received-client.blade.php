<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
body {
    background:#F1F1F1;
}

.container {
    background:white;
    border-radius:4px;
    margin-top:50px;
    margin-bottom:50px;
}

.header {
    padding-top:30px;
    padding-bottom:30px;
}

table {
    background: #fafafa;
}
</style>
<body>
    <div class="container">
        <div class="row header">
            <div class="col-md-12">
                <h2>Pemesanan Berhasil, terimakasih telah berbelanja di toko kami</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <p>
                Segera selesaikan pembayaran agar pesanan kamu dapat segera diproses.
            </p>
            <p>
                Kode Pemesanan : <b> {{ $order->kode_pemesanan }} </b>
            </p>
            <p>
                Total Pesanan : <b> {{ $order->getFormattedTotal() }} </b>
            </p>
            <p>Berikut adalah detail pesanan anda: </p>
            <table class="table">
                <tr>
                    <td>#</td>
                    <td>Nama Barang</td>
                    <td>Qty</td>
                    <td>Harga</td>
                </tr>
                @foreach ($order->details as $detail)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $detail->barang->nama_barang }}</td>
                        <td>{{ $detail->qty }}</td>
                        <td>{{ $detail->barang->getRealPrice() }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3"><strong>Ongkos Kirim</strong></td>
                    <td>{{ $order->shipping->getShippingPrice() }}</td>
                </tr>
                <tr>
                    <td colspan="3"><strong>Total</strong></td>
                    <td>{{ $order->getFormattedTotal() }}</td>
                </tr>
            </table>
            </div>
        </div>
    </div>
    
</body>
