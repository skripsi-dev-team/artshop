	<div class="categories">
		<h2>Kategori Barang</h2>
		<ul class="cate">
			@foreach(listCategory() as $list)
				<li><a href="{{ route('client.kategori', ['id' => $list->id_kategori]) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i>{{ ucwords($list->nama_kategori) }}</a></li>
			@endforeach
		</ul>
	</div>	