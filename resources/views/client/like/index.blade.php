@extends('client.app')

@section('title')
Profile
@endsection

@section('content')
<div class="container like-page" style="padding: 30px 0">
    @if (session()->has('success'))
        <div class="alert alert-success">
            <p>{{ session()->get('success') }}</p>
        </div>
    @endif
    @forelse ($items as $item)
        <h3>Barang yang disukai</h3>
        <br>
        <div class="row liked-row">
            <div class="like-item">
                <div class="col-md-3">
                    <img src="{{ $item->getImageUrl() }}" alt="{{ $item->foto }}" srcset="">
                </div>
                <div class="col-md-9">
                    <h2 class="item-title">{{ $item->nama_barang }}</h2>
                    <h4 class="item-price">
                        @if($item->diskon == 0 || $item->diskon == null)
                            Rp. {{ number_format($item->harga) }}
                        @else
                            Rp. {{ number_format($item->getDiscountPrice()) }}
                            <span>Rp. {{ number_format($item->harga) }}</span>
                        @endif
                    </h4>
                    <div class="snipcart-details top_brand_home_details">
                        <input type="button" data-id="{{ $item->id_barang }}" data-url="{{ route('api.cart.add') }}" class="button add-to-cart" value="Add to cart">
                        <a style="margin-left: 15px;" href="#" class="btn btn-danger remove-like" data-url="{{ route('api.like.remove', $item->id_barang) }}"> <i class="fa fa-trash"></i> Hapus dari list</a>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="no-like">
            <div class="row">
                Anda belum memiliki barang yang disukai        
            </div>
        </div>
    @endforelse
    
</div>
@endsection

@push('scripts')
<script>
(function($) {
    $('.remove-like').on('click', function(e) {
        e.preventDefault();

        if (confirm('Anda akan menghapus barang dari daftar suka, lanjutkan?')) {
            $.post($(this).attr('data-url'), function(response) {
                if (response.status == 'ok') {
                    location.reload();
                }
            });
        }
    })
})(jQuery)
</script>
@endpush