@extends('client.app')

@section('title')
Checkout
@endsection


@section('styles')
<style>
table.order-summary > tr > td {
    border-bottom: 1px solid rgba(200, 200, 200, .45);
}
</style>
@endsection

@section('content')
<div class="checkout">
    <div class="container">
        <div class="row">
            <h2>Terimakasih telah melakukan pemesanan</h2>   
        </div>
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        <div class="row">
            <table class="table order-summary">
                <tr>
                    <td><b>No Order :</b></td>
                    <td>{{ $order->kode_pemesanan }}</td>
                    <td><b>Tanggal Order :</b></td>
                    <td>{{ $order->tanggal }}</td>
                    <td><b>Total</b></td>
                    <td>{{ 'Rp. '.number_format($order->total_nominal) }}</td>
                    <td><b>Email :</b></td>
                    <td>{{ $order->customer->email }}</td>
                    <td><b>Name :</b></td>
                    <td>{{ $order->customer->name }}</td>                    
                </tr>
            </table>
        </div>
        <div class="row">
            <table class="table">
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>QTY</th>
                <th>Foto</th>
    
                @foreach ($order->details as $item)
                    <tr>
                        <td>{{ $item->barang->nama_barang }}</td>
                        <td>{{ $item->barang->getRealPrice() }}</td>
                        <td>{{ $item->qty }}</td>
                        <td>
                            <img src="{{ $item->barang->getImageUrl() }}" width="150">
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        @if ($order->status_bayar == 0)
            <div class="row">
                <form action="https://staging.doku.com/Suite/Receive" method="POST">
                    <input type="hidden" name="MALLID" value="{{ env('MALLID', null) }}">
                    <input type="hidden" name="CHAINMERCHANT" value="NA">
                    <input type="hidden" name="SESSIONID" value="{{ $order->id_pemesanan }}">
                    <input type="hidden" name="AMOUNT" value="{{ $order->total_nominal.'.00' }}">
                    <input type="hidden" name="PURCHASEAMOUNT" value="{{ $order->total_nominal.'.00' }}">
                    <input type="hidden" name="TRANSIDMERCHANT" value="{{ $order->kode_pemesanan }}">
                    <input type="hidden" name="WORDS" value="{{ getWordsKey($order->total_nominal, $order->kode_pemesanan) }}">
                    <input type="hidden" name="REQUESTDATETIME" value="{{ now()->format('YmdHis') }}">
                    <input type="hidden" name="CURRENCY" value="360">
                    <input type="hidden" name="PURCHASECURRENCY" value="360">
                    <input type="hidden" name="BASKET" value="{{ createBasket($order) }}">
                    <input type="hidden" name="NAME" value="{{ $order->customer->name }}">
                    <input type="hidden" name="EMAIL" value="{{ $order->customer->email }}">
                    <input type="submit" value="Bayar" class="btn btn-primary">
                </form>
            </div>
        @else
            <div class="row">
                <div class="alert alert-success">
                    Anda telah melakukan pembayaran.
                </div>
            </div>
        @endif
    </div> 
</div>
@endsection