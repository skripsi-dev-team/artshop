@extends('client.app')

@section('title')
Checkout
@endsection

@section('content')
<div class="container" style="padding: 30px 0">
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('pelanggan.order') }}" method="POST" class="form">
                @csrf
                <h2 class="form-section">Data Diri Pemesan</h2>
                <div class="row">

                    <input type="hidden" name="pelanggan_id" value="{{ $user->id_pelanggan }}">
                    <input type="hidden" name="estimasi">
                    <input type="hidden" name="total_nominal">
                    <input type="hidden" name="ongkos_kirim">
                    <input type="hidden" name="kota">
                    <input type="hidden" name="provinsi">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Name">Nama</label>
                            <input type="text" name="name" id="Name" disabled class="form-control" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="Email">Email</label>
                            <input type="text" name="email" id="Email" disabled class="form-control" value="{{ $user->email }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="NoTelp">No Telp</label>
                            <input type="text" name="no_telp" id="NoTelp" disabled class="form-control" value="{{ $user->no_telp }}">
                        </div>
                    </div>
                </div>
                
        
                <h2 class="form-section">
                    Pengiriman
                </h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Province">Pilih Provinsi</label>
                            <select id="Provinsi" class="form-control">
                                <option value="0">Pilih Provinsi</option>
                            </select>
                            @if ($errors->has('provinsi'))
                                <span style="color: red">{{ $errors->first('provinsi') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Kota">Pilih Kota</label>
                            <select id="Kota" class="form-control">
                                <option value="0">Pilih Kota</option>
                           
                            </select>
                            @if ($errors->has('kota'))
                                <span style="color: red">{{ $errors->first('kota') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Ekspedisi">Jasa Pengiriman</label>
                            <select name="jasa_kirim" id="Ekspedisi" class="form-control">
                                <option value="0">Pilih Jasa Ekspedisi</option>
                                <option value="jne">JNE</option>
                                <option value="pos">POS</option>
                                <option value="tiki">TIKI</option>
                            </select>
                            @if ($errors->has('jasa_kirim'))
                                <span style="color: red">{{ $errors->first('jasa_kirim') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="PaketPengiriman">Paket Pengiriman</label>
                            <select name="paket_pengiriman" id="PaketPengiriman" class="form-control"></select>
                            @if ($errors->has('alamat_kirim'))
                                <span style="color: red">{{ $errors->first('alamat_kirim') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="AlamatKirim">Alamat Pengiriman</label>
                            <textarea name="alamat_kirim" id="" cols="30" rows="10" class="form-control">{{ $user->alamat }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="ORDER" class="btn btn-primary pull-right">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <h2 class="product-summary">Ringkasan Pemesanan</h2>
            <table class="table table-striped summary">
                <thead>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Qty</th>
                </thead>
                <tbody>
                    @foreach (getCart()->all() as $item)
                    <tr>
                        <td>{{ $item['item_name'] }}</td>
                        <td>{{ 'Rp. '.number_format($item['price']) }}</td>
                        <td>{{ $item['qty'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="2"><strong>Subtotal</strong></td>
                        <td id="subTotal">{{ getCartTotal() }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Ongkos Kirim</strong></td>
                        <td id="shippingCharge">Rp. 0</td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Total</strong></td>
                        <td id="Total">{{ getCartTotal() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
@endsection

@push('scripts')
<script>
(function($){

    function formatNumber(currency, number) {
        return currency + number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function fetchProvince() {
        $.get('api/provinces', function(response) {
            const provinces = response.rajaongkir.results;
            provinces.forEach(function(item) {
                $('#Provinsi').append('<option value="'+item.province_id+'">'+item.province+'</option>');
            });
        });
    }

    function fetchCity() {
        $(document).on('change', '#Provinsi', function(e) {
            $.get('api/cities?province='+$(this).val(), function(response) {
                const cities = response.rajaongkir.results;
                const city = $('#Kota');

                city.html('');

                cities.forEach(function(item) {
                    city.append('<option value="'+item.city_id+'">'+item.city_name+'</option>');
                });

                var val = $('#Kota').find('option:selected').text();
                $('input[name="kota"]').val(val); 
            });

            var val = $(this).find('option:selected').text();
            $('input[name="provinsi"]').val(val); 
        })
    }

    function fetchCourier() {
        $(document).on('change', '#Ekspedisi', function(e) {
            $.get('api/cost?destination='+$('#Kota').val()+'&courier='+$(this).val(), function(response) {
                const package = $('#PaketPengiriman');
                const services = response.rajaongkir.results[0].costs;
                var html = '';

                services.forEach(function(item, index) {
                    html += '<option value="'+item.service+'" data-price="'+item.cost[0].value+'" data-etd="'+item.cost[0].etd+'">'+item.service+'( '+item.cost[0].value+' ) - '+item.cost[0].etd.replace(/[a-z A-Z]/g, '')+' hari</option>';
                });
                package.html(html);

                calculateTotal();

            });
        })
    }

    function calculateTotal() {
        const option = $('#PaketPengiriman').find('option:selected');
        const etd = option.attr('data-etd');
        const price = parseInt(option.attr('data-price'));
        const total = price + parseInt($('#subTotal').text().replace(/[a-z A-Z\s\n,.]/g, ''));
        

        $('input[name="estimasi"]').val(etd);
        $('input[name="ongkos_kirim"]').val(price);
        $('input[name="total_nominal"]').val(total);

        $('#shippingCharge').text(formatNumber('Rp. ', price));
        $('#Total').text(formatNumber('Rp. ', total));
    }

    $(document).on('change', '#PaketPengiriman', function(e) {
        calculateTotal();
    });

    $(document).on('change', '#Kota', function(e) {
        var val = $(this).find('option:selected').text();
        $('input[name="kota"]').val(val); 
    });

    fetchProvince();
    fetchCity();
    fetchCourier();
})(jQuery)
</script>
@endpush