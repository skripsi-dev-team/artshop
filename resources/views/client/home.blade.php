@extends('client.app')

@section('title')
Selamat Datang
@endsection

@section('content')
	<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner my-carousel" role="listbox">
			<div class="item active">
				<p>Musik adalah abadi karena bersifat cita – cita dan tak terhingga sebab musik adalah puisi bunyi yang mengawali penciptaan ide – ide musikal selanjutnya <br>
				- Wagner</p>
				<img class="first-slide" src="{{ asset('client/images/banner-1.jpg') }}" alt="First slide">
			</div>
			<div class="item">
				<p>Hanya musik yang memberikan arti dalam hidup manusia. Musik dapat menjadi tempat pelarian manusia dari kenyataan hidup</br>
				- Friedrich Nietzsche</p>
				<img class="second-slide " src="{{ asset('client/images/banner-2.jpg') }}" alt="Second slide">
			</div>
			<div class="item">
				<p>Kami menyediakan media untuk mengekspresikan luapan rasa melalui alat musik yang sesuai dengan diri anda<br>
				-Surya Bali Art</p>
				<img class="third-slide " src="{{ asset('client/images/banner-3.jpg') }}" alt="Third slide">
			</div>
		</div>
	</div>
	<!-- /.carousel -->	

	<!-- HOME TENTANG KAMI -->
	<div class="container home-about top-brands">
		<div class="row">
			<div class="col-md-12">
				<h2>Tentang Kami</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">				
				<p>“Who We Are ?</p>
				<p>Surya Bali Art adalah sebuah toko yang menjual alat musik handmade yang mengutamakan kualitas dan kepuasan pelanggan. Toko ini terletak di Jalan Raya Patih Jelatik No. 444 Legian, dengan posisi yang strategis membuat Surya Bali Art memiliki peluang bisnis yang menjanjikan, selain muncul dalam bentuk toko fisik, Surya Bali Art juga muncul dalam bentuk toko online untuk menjangkau konsumen yang menginginkan alat musik dengan harga yang bersaing dan kualitas yang terbaik.”</p>

			</div>
		</div>
	</div>
	
	<!-- top-brands -->
	<div class="top-brands">
		<div class="container">
			<h2>Tawaran Terbaik Kami</h2>
			<div class="grid_3 grid_5">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
							<div class="agile-tp">
								<h5>Penawaran khusus</h5>
								<p class="w3l-ad">Dapatkan produk terbaru dengan kualitas terbaik dan termurah hanya disini.</p>
							</div>
							<div class="agile_top_brands_grids">
							@foreach($barang as $data)
								<div class="col-md-4 top_brand_left">
									<div class="hover14 column">
										<div class="agile_top_brand_left_grid">
											<div class="agile_top_brand_left_grid_pos">
												@if($data->diskon != 0 || $data->diskon != null)
													<span class="discount-badge">Discount {{ $data->diskon }}%</span>
												@endif
											</div>
											<div class="agile_top_brand_left_grid1">
												<figure>
													<div class="snipcart-item block" >
														<div class="snipcart-thumb">
															<a href="{{ route('client.detail_barang', ['slug' => $data->slug ]) }}"><img alt="{{ $data->foto }}" src="{{ $data->getImageUrl() }}" class="img-responsive" /></a>		
															<p>{{ $data->nama_barang }}</p>																
															<h4>
																@if($data->diskon == 0 || $data->diskon == null)
																	Rp. {{ number_format($data->harga) }}
																@else
																	Rp. {{ number_format($data->getDiscountPrice()) }}
																	<span>Rp. {{ number_format($data->harga) }}</span>
																@endif																 
															</h4>
														</div>
														<div class="snipcart-details top_brand_home_details">
															<input type="button" value="Add to cart" data-id="{{ $data->id_barang }}" data-url="{{ route('api.cart.add') }}" class="button add-to-cart">
														</div>
													</div>
												</figure>
											</div>
										</div>
									</div>
								</div>
							@endforeach
								
								<div class="clearfix"> </div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //top-brands -->

	<!-- OUR SERVICE -->
	<div class="container home-service top-brands">
		<div class="row">
			<div class="col-md-12">
				<h2>Layanan Kami</h2>
			</div>
		</div>
		<div class="row service-content">
			<div class="col-md-4">
				<h4>Harga Termurah</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
			<div class="col-md-4">
				<h4>Produk Original</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
			<div class="col-md-4">
				<h4>Bergaransi</h4>
				<div class="service-icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
		</div>
	</div>

 	
	
    
@endsection