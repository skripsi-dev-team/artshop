@extends('admin.app')

@section('title')
Tambah User
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4>Form User</h4>
                <hr>
                <form action="{{ route('user.store') }}" method="post">
                @csrf
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Nama User</label>
                            <input type="text" class="form-control" id="name" placeholder="Nama" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">No Telp</label>
                            <input type="text" class="form-control" id="no_telp" placeholder="No Telp" name="no_telp" value="{{ old('no_telp') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Konfirmasi Password</label>
                            <input type="password" class="form-control" id="password_confirmation" placeholder="Password Konfirmasi" name="password_confirmation" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Level</label>
                            <select name="level" class="form-control">
                                <option value=""> - Pilih Level - </option>
                                <option value="0">Admin</option>
                                <option value="1">Owner</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection