@extends('admin.app')

@section('title')
User
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
        
                <a href="{{ route('user.create') }}" class="btn btn-outline-primary">Tambah User</a>
                <br><br>
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th>Level</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ ($user->level == 1) ? 'Owner' : 'Admin' }}</td>
                                    
                                        <!-- form (for delete) -->
                                        <form action="{{ route('user.destroy', ['id' => $user->id_user] ) }}" id="delete-user" method="post">
                                            <!-- EDIT BUTTON -->
                                            <td>
                                                <a href="{{ route('user.edit', ['id' => $user->id_user]) }}" class="btn btn-sm btn-warning">Edit</a>
                                           
                                            <!-- DELET BUTTON -->
                                           
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus user?')">
                                                    Hapus 
                                                </button>
                                            </td>
                                            @csrf 
                                            @method('DELETE')
                                        </form>
                                        <!-- END FORM -->
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
                {!! $users->links() !!}
            </div>
        </div>
    </div>
</div> 
@endsection