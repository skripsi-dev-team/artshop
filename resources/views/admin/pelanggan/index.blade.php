@extends('admin.app')

@section('title')
Data Pelanggan
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Pelanggan</th>
                                <th scope="">Email</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">No Telp</th>
                                <th scope="col">Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pelanggan as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->alamat }}</td>
                                    <td>{{ $data->no_telp }}</td>
                                        <!-- form (for delete) -->
                                        <form action="{{ route('api.pelanggan.delete', ['id' => $data->id_pelanggan] ) }}" method="post" class="delete-pelanggan">
                                            <td>
                                                <!-- DELETE BUTTON -->
                                                <button type="submit" class="btn btn-danger btn-sm" >
                                                    Hapus 
                                                </button>
                                            </td>
                                            @csrf 
                                            @method('DELETE')
                                        </form>
                                        <!-- END FORM -->
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection


@push('scripts')
<script>

(function($){
    $('.delete-pelanggan').on('submit', function(e) {
        e.preventDefault();
        var self = $(this);
        if (confirm('Hapus permanen barang ini?')) {
            self[0][0].innerText = 'Sedang Menghapus...';
            
            
            $.ajax({
                url: self.attr('action'),
                method: 'DELETE',
                success: function(res) {
                    location.reload();
                    toastr.success('Pelanggan berhasil dihapus');
                },
                error: function(err) {
                    self[0][0].innerText = 'Hapus';
                    toastr.error('Pelanggan tidak bisa dihapus, karena sedang digunakan dimodul lain!', { timeOut: 300 });
                }
            });
        }

    })
})(jQuery)

</script>
@endpush