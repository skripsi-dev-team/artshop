        @guest
            <div>
        @else
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item {{ urlHasPrefix('dashboard') ? 'selected' : '' }} "> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('admin.home') }}"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item {{ urlHasPrefix('kategori') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('kategori.index') }}"><i class="mdi mdi-equal-box"></i><span class="hide-menu">Kategori Barang</span></a></li>
                        <li class="sidebar-item {{ urlHasPrefix('barang') ? 'active' : '' }}"> 
                        
                            <!-- <a class="sidebar-link has-arrow waves-effect waves-dark sidebar-link" href="#">
                            <i class="fas fa-box"></i><span class="hide-menu">Manage Barang</span></a> -->
                            <!-- <ul class="collapse first-level {{ urlHasPrefix('stok') ? 'selected' : '' }}">
                                <li class="sidebar-item"> -->
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('barang.index') }}">
                                        <i class="fas fa-box"></i><span class="hide-menu">Barang</span></a>
                                <!-- </li> -->
                                <!-- <li class="sidebar-item {{ urlHasPrefix('trash') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('barang.trash') }}"><i class="fas fa-trash"></i><span class="hide-menu">Trash Barang</span></a></li> -->
                            <!-- </ul> -->
                        </li>
                        @if(Auth::user()->level == 1) 
                        <li class="sidebar-item {{ urlHasPrefix('user') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('user.index') }}"><i class="fas fa-user"></i><span class="hide-menu">User</span></a></li>
                        @endif
                        <li class="sidebar-item {{ urlHasPrefix('pelanggan') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('pelanggan.index') }}"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Pelanggan</span></a></li>
                        <li class="sidebar-item {{ urlHasPrefix('pemesanan') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('pemesanan.index') }}"><i class="mdi mdi-cart"></i><span class="hide-menu">Pemesanan</span></a></li>
                        <li class="sidebar-item {{ urlHasPrefix('pembayaran') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('pembayaran.index') }}"><i class="fas fa-money-bill-wave"></i><span class="hide-menu">Pembayaran</span></a></li>
                        
                        @if(Auth::user()->level == 1) 
                            <li class="sidebar-item {{ urlHasPrefix('laporan-penjualan') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('admin/laporan-penjualan') }}"><i class="fa fa-list"></i><span class="hide-menu">Laporan Penjualan</span></a></li>
                            <li class="sidebar-item {{ urlHasPrefix('laporan-stock') ? 'selected' : '' }}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('admin/laporan-stock') }}"><i class="fa fa-list"></i><span class="hide-menu">Laporan Stock</span></a></li>                    
                        @endif
                    
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @endguest