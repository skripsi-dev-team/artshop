@extends('admin.app')

@section('title')
Laporan Penjualan
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4>Tampilkan Laporan Penjualan</h4>
                <form action="{{ route('laporan.penjualan') }}" class="custDate" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Dari Tanggal</label>
                            <input type="date" class="form-control firstDate" name="dari" required>
                        </div>
                        <div class="col-md-6">
                            <label>Sampai Tanggal</label>
                            <input type="date" class="form-control secondDate" name="sampai" required>
                        </div>
                        <div class="col-md-12 mt-3">
    
                            <input type="submit" value="Cari" name="cari" class="btn btn-primary float-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if($laporan != null)
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <p><a href='{{ url("admin/laporan-penjualan/print?dari=$_GET[dari]&sampai=$_GET[sampai]") }}'>Cetak PDF</a></p>
                <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode</th>
                            <th>Tanggal</th>
                            <th>Pelanggan</th>
                            <th>Total</th>
                            <th>Status Pemesanan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($laporan as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $data->kode_pemesanan }}</td>
                                <td>{{ $data->tanggal }}</td>
                                <td>{{ $data->customer->name }}</td>
                                <td>Rp. {{ number_format($data->total_nominal) }}</td>
                                <td>{{ $data->getStatusText() }}</td>
                                    
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endif

@endsection

@push('scripts')

<script>
    
    $('.custDate').change(function () {
		var firstDate = $('.firstDate').val();
		var secondDate = '';

		if (!$('.secondDate').val()) {
			$('.secondDate').attr('min', firstDate);
		} else {
			secondDate = $('.secondDate').val();
			if (firstDate > secondDate) {
				$('.secondDate').attr('min', firstDate);
				$('.secondDate').val(firstDate);
			} else {
				$('.secondDate').attr('min', firstDate);
			}
		}
	});
</script>
@endpush