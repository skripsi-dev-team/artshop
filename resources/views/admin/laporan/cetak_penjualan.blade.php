<style>

body {
    font-family: "Arial";
}

.body-print {
    max-width: 1280px;
    margin:auto;
}

.header-print {
    text-align:center;
}

.content-print {
    margin-top: 30px;
}

.title {
    font-weight:bold;
    font-size:18px;
}

table {
  width: 100%;
  border-collapse: collapse;
}

th {
  height: 30px;
}

table, th, td {
  border: 1px solid black;
  padding:8px;
}

table tr td:first-child {
    text-align:center;
}


</style>
<div class="body-print">
    <div class="header-print">
        <h2>Surya Bali Art</h2>
        <p>Jl. Patih Jelatik No. 444, Legian Badung, Bali, Indonesia.</p>
        <p>Telp: 081916401820 | Email: suryabaliart@gmail.com</p>
    </div>
    <hr>
    <div class="content-print">
        
        <table class="table-print">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Tanggal</th>
                    <th>Pelanggan</th>
                    <th>Total</th>
                    <th>Status Pemesanan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($laporan as $data)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $data->kode_pemesanan }}</td>
                        <td>{{ $data->tanggal }}</td>
                        <td>{{ $data->customer->name }}</td>
                        <td>Rp. {{ number_format($data->total_nominal) }}</td>
                        <td>{{ $data->getStatusText() }}</td>
                            
                    </tr>
                @endforeach
            </tbody>
        </table>
       
    </div>
</div>