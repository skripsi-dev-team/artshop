<style>

body {
    font-family: "Arial";
}

.body-print {
    max-width: 1280px;
    margin:auto;
}

.header-print {
    text-align:center;
}

.content-print {
    margin-top: 30px;
}

.title {
    font-weight:bold;
    font-size:18px;
}

table {
  width: 100%;
  border-collapse: collapse;
}

th {
  height: 30px;
}

table, th, td {
  border: 1px solid black;
  padding:8px;
}

table tr td:first-child {
    text-align:center;
}


</style>
<div class="body-print">
    <div class="header-print">
        <h2>Surya Bali Art</h2>
        <p>Jl. Patih Jelatik No. 444, Legian Badung, Bali, Indonesia.</p>
        <p>Telp: 081916401820 | Email: suryabaliart@gmail.com</p>
    </div>
    <hr>
    <div class="content-print">
        <table class="table" id="datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>SKU</th>
                    <th>Nama Barang</th>
                    <th>Stock</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($barang as $data)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $data->sku }}</td>
                        <td>{{ $data->nama_barang }}</td>
                        <td>{{ $data->stock }}</td>
                            
                    </tr>
                @endforeach
                
            </tbody>
        </table>
       
    </div>
</div>