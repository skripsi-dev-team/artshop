@extends('admin.app')

@section('title')
Laporan Stock Barang
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <p><a href='{{ url("admin/laporan-stock/print") }}'>Cetak PDF</a></p>
                <table class="table" id="datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SKU</th>
                            <th>Nama Barang</th>
                            <th>Stock</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($barang as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $data->sku }}</td>
                                <td>{{ $data->nama_barang }}</td>
                                <td>{{ $data->stock }}</td>
                                    
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection