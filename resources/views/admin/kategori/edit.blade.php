@extends('admin.app')

@section('title')
Edit Kategori Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4>Form Edit Kategori Barang</h4>
                <hr>
                <form action="{{ route('kategori.update', ['id_kategori' => $kategori->id_kategori]) }}" method="post">
                @csrf
                @method('PUT')
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="fname" placeholder="Nama Kategori" name="nama_kategori" value="{{ $kategori->nama_kategori }}">
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection