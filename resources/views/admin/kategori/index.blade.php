@extends('admin.app')

@section('title')
Kategori Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
        
                <a href="{{ route('kategori.create') }}" class="btn btn-outline-primary">Tambah Kategori</a>
                <br><br>
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Kategori</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$data->nama_kategori}}</td>
                                    
                                        <!-- form (for delete) -->
                                        <form action="{{ route('kategori.destroy', ['id' => $data->id_kategori] ) }}" id="delete-kategori" method="post">
                                            <!-- EDIT BUTTON -->
                                            <td>
                                                <a href="{{ route('kategori.edit', ['id' => $data->id_kategori]) }}" class="btn btn-sm btn-warning">Edit</a>
                                           
                                            <!-- DELET BUTTON -->
                                           
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus Data?')">
                                                    Hapus 
                                                </button>
                                            </td>
                                            @csrf 
                                            @method('DELETE')
                                        </form>
                                        <!-- END FORM -->
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection