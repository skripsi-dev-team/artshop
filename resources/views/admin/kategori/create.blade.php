@extends('admin.app')

@section('title')
Tambah Kategori Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4>Form Kategori Barang</h4>
                <hr>
                <form action="{{ route('kategori.store') }}" method="post">
                @csrf
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="fname" placeholder="Nama Kategori" name="nama_kategori" value="{{ old('nama_kategori') }}">
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection