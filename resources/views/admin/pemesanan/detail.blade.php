@extends('admin.app')

@section('title')
Detail Pemesanan 
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td>Kode Pemesanan</td>
                        <td>:</td>
                        <td>{{ $pemesanan->kode_pemesanan }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ $pemesanan->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>Nama Pemesan</td>
                        <td>:</td>
                        <td>{{ $pemesanan->customer->name }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ $pemesanan->customer->email }}</td>
                    </tr>
                    <tr>
                        <td>Status Pemesanan</td>
                        <td>:</td>
                        <td>{!! $pemesanan->statusBadge() !!}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">

                <form action="{{ route('pemesanan.update', ['id' => $pemesanan->id_pemesanan]) }}" method="post">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label>Ubah Status Pemesanan</label>
                        <select name="status_pemesanan" class="form-control">
                            @for($i = 0; $i <= 5; $i++)
                                <option value="{{ $i }}" {{ ($pemesanan->status_pemesanan == $i) ? 'selected' : '' }}>{{ $pemesanan->statusText($i) }}</option>
                            @endfor
                        </select>
                    </div>
                    <input type="submit" value="Simpan" class="float-right">
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>QTY</th>
                                <th>Harga</th>
                                <th>Total</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($detail_pemesanan as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $data->barang->nama_barang }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>{{ $data->barang->getRealPrice() }}</td>
                                    <td>Rp. {{ number_format($data->barang->getTotalQty($data->qty)) }}</td>
                                       
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('scripts')

<script src="{{ asset('DataTables/datatables.min.js') }}"></script>

<script>

$('#datatable').DataTable();

</script>

@endpush

