@extends('admin.app')

@section('title')
Data Pemesanan
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Pelanggan</th>
                                <th scope="col">Total</th>
                                <th scope="col">Status Bayar</th>
                                <th scope="col">Status Pemesanan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pemesanan as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $data->kode_pemesanan }}</td>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>{{ $data->customer->name }}</td>
                                    <td>{{ $data->total_nominal }}</td>
                                    <td>{{ $data->status_bayar == 0 ? 'Belum Dibayar' : 'Sudah Dibayar' }}</td>
                                    <td>{!! $data->statusBadge() !!}</td>
                                        <!-- form (for delete) -->
                                        <!-- <form action="{{ route('api.pelanggan.delete', ['id' => $data->id_pelanggan] ) }}" method="post" class="delete-pelanggan"> -->
                                            <td>
                                                <a href="{{ route('pemesanan.show', ['id' => $data->id_pemesanan]) }}" class="btn btn-sm btn-primary ">Detail</a>
                                                
                                            </td>
                                            <!-- @csrf 
                                            @method('DELETE')
                                        </form> -->
                                        <!-- END FORM -->
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('scripts')

<script src="{{ asset('DataTables/datatables.min.js') }}"></script>

<script>

$('#datatable').DataTable();

</script>

@endpush

