@extends('admin.app')

@section('title')
Data Stok Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Jumlah Stok</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                            @foreach($stok as $data)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{ $data->nama_barang }}</td>
                                    <td>{{ showStock($data->id_barang) }}</td>
                                    <!-- EDIT BUTTON -->
                                    <td>
                                        <a href="{{ route('barang.show', ['id' => $data->id_barang]) }}" class="btn btn-sm btn-primary">Lihat Detail</a>
                                        <a href="{{ route('stok.edit', ['id' => $data->id_barang]) }}" class="btn btn-sm btn-warning">Edit Stok</a>
                                    </td>       
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
                @if ($stok instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $stok->links() }}
                @endif
            </div>
        </div>
    </div>
</div> 
@endsection