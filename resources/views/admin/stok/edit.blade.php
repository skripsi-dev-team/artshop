@extends('admin.app')

@section('title')
Edit Stok Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4>Form Edit Stok Barang</h4>
                <hr>
                <form action="{{ route('stok.update', ['id' => $barang->id_barang]) }}" method="post">
                @csrf
                @method('PUT')
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="id_barang">Nama Barang</label>
                            <input type="text" class="form-control" placeholder="Nama Kategori" name="nama_kategori" value="{{ $barang->nama_barang }}" readonly>
                            <input type="hidden" name="barang_id" value="{{ $barang->id_barang }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="jumlah">Jumlah Stok</label>
                            <input type="text" class="form-control" placeholder="Masukan jumlah stok sekarang" name="jumlah" value="{{ showStock($barang->id_barang) }}">
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection