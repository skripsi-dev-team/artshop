@extends('admin.app')

@section('title')
Detail Barang {{ $barang->nama_barang }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{ $barang->nama_barang }}</h4>
                <hr>
                <img src="<?= asset('storage/gambar_produk').'/'.$barang->foto ?>" alt="{{ $barang->foto }}" class="img-fluid">
                <table class="table">
                    <tr>
                        <td>SKU</td>
                        <td>:</td>
                        <td>{{ $barang->sku }}</td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td>:</td>
                        <td>{{ $barang->checkDiscount() }}</td>
                    </tr>
                    <tr>
                        <td>SLUG</td>
                        <td>:</td>
                        <td>{{ $barang->slug }}</td>
                    </tr>
                    <tr>
                        <td>Stock</td>
                        <td>:</td>
                        <td>{{ $barang->stock }}</td>
                    </tr>
                    <tr>
                        <td>Berat</td>
                        <td>:</td>
                        <td>{{ $barang->berat }} gram</td>
                    </tr>
                    <tr>
                        <td>Panjang</td>
                        <td>:</td>
                        <td>{{ $barang->panjang }} CM</td>
                    </tr>
                    <tr>
                        <td>Lebar</td>
                        <td>:</td>
                        <td>{{ $barang->lebar }} CM</td>
                    </tr>
                    <tr>
                        <td>Tinggi</td>
                        <td>:</td>
                        <td>{{ $barang->tinggi }} CM</td>
                    </tr>
                </table>
                <p><strong>Deskripsi Barang:</strong></p>
                    <?= $barang->deskripsi ?>
            </div>
        </div>
    </div>
</div>
@endsection

