@extends('admin.app')

@section('title')
Tempat Sampah Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">SKU</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Gambar</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($barang as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$data->nama_barang}}</td>
                                    <td>{{$data->category->nama_kategori}}</td>
                                    <td>{{ $data->sku }}</td>
                                    <td>{{ $data->harga }}</td>
                                    <td><img src="<?= asset('storage/gambar_produk/small').'/'.$data->foto ?>" alt="{{ $data->foto }}" width="100px"></td>
                                        <!-- form (for delete) -->
                                        <form action="{{ route('api.barang.delete_permanent', ['id' => $data->id_barang] ) }}" class="delete-barang" method="post">
                                            <!-- EDIT BUTTON -->
                                            <td>
                                                <a href="{{ route('barang.restore', ['id' => $data->id_barang]) }}" class="btn btn-sm btn-primary btn-product">Restore Barang</a>
                                                <!-- DELETE BUTTON -->
                                                <button type="submit" class="btn btn-danger btn-sm btn-product">
                                                    Hapus Permanent 
                                                </button>
                                            </td>
                                        </form>
                                        <!-- END FORM -->
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('scripts')
<script>
(function($){
    $('.delete-barang').on('submit', function(e) {
        e.preventDefault();
        var self = $(this);
        if (confirm('Hapus permanen barang ini?')) {
            self[0][0].innerText = 'Sedang Menghapus...';
            
            
            $.ajax({
                url: self.attr('action'),
                method: 'DELETE',
                success: function(res) {
                    location.reload();
                },
                error: function(err) {
                    self[0][0].innerText = 'Hapus Permanen';
                    toastr.error('Barang tidak bisa dihapus permanen, karena sedang digunakan dimodul lain!', { timeOut: 300 });
                }
            });
        }

    })
})(jQuery)
</script>
@endpush