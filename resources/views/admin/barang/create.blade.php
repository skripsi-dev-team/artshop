@extends('admin.app')

@section('title')
Tambah Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4>Form Barang</h4>
                <hr>
                <form action="{{ route('barang.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" id="nama_barang" class="form-control" placeholder="Masukan Nama Barang" name="nama_barang" value="{{ old('nama_barang') }}" onkeyup="makeSlug()">
                        </div>
                        <div class="col-sm-6">
                            <label for="kategori_id">Kategori</label>
                            <select name="kategori_id" class="form-control" required>
                                <option value=""> - Pilih Kategori - </option>
                                @foreach($kategori as $option)
                                <option value="{{ $option->id_kategori }}">{{ $option->nama_kategori }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="sku">SKU</label>
                            <input type="text" class="form-control" placeholder="Masukan SKU" name="sku" value="{{ old('sku') }}">
                        </div>
                        <div class="col-sm-6">
                        <label for="harga">Harga</label>
                            <input type="number" class="form-control" placeholder="Masukan Harga" name="harga" value="{{ old('harga') }}">
                        </div>
                        <input type="hidden" id="slug" class="form-control" placeholder="Masukan Slug" name="slug" value="{{ old('slug') }}">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="sku">Stock</label>
                            <input type="number" class="form-control" placeholder="Masukan Stock" name="stock" value="{{ old('stock') }}">
                        </div>
                        <div class="col-sm-6">
                        <label for="harga">Diskon</label>
                            <div class="input-group">
                                <input type="number" class="form-control" placeholder="Masukan Diskon" name="diskon" value="{{ old('diskon') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">Berat</label>
                            <div class="input-group">
                                <input type="number" name="berat" id="" class="form-control" value="{{ old('berat') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">gram</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="">Panjang</label>
                            <div class="input-group">
                                <input type="number" name="panjang" id="" class="form-control" value="{{ old('panjang') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">CM</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="">Lebar</label>
                            <div class="input-group">
                                <input type="number" name="lebar" id="" class="form-control" value="{{ old('lebar') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">CM</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="">Tinggi</label>
                            <div class="input-group">
                                <input type="number" name="tinggi" id="" class="form-control" value="{{ old('tinggi') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">CM</span>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="gambar">Gambar</label>
                            <input type="file" class="form-control" placeholder="Masukan Gambar" name="foto" value="{{ old('gambar') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="deskripsi">Deskripsi</label>
                            <textarea id="editor1" name="deskripsi" class="form-control" id="editor" rows="4" placeholder="Masukan deksripsi yang menarik disini"></textarea>
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('scripts')

<script>

function makeSlug(){
    var barangName = $('#nama_barang').val();
    var slugName = barangName.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1' );
</script>


@endpush
