@extends('admin.app')

@section('title')
Barang
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
        
                <a href="{{ route('barang.create') }}" class="btn btn-outline-primary">Tambah Barang</a>
                <br><br>
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">SKU</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Gambar</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($barang as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->sku }}</td>
                                    <td>{{ $data->nama_barang}}</td>
                                    <td>{{ $data->category->nama_kategori }}</td>
                                    <td>{{ $data->stock }}</td>
                                    <td>{{ $data->checkDiscount() }}</td>
                                    <td><img src="<?= asset('storage/gambar_produk/small').'/'.$data->foto ?>" alt="{{ $data->foto }}" width="100px"></td>
                                        <!-- form (for delete) -->
                                        <form action="{{ route('barang.destroy', ['id' => $data->id_barang] ) }}" id="delete-barang" method="post">
                                            <!-- EDIT BUTTON -->
                                            <td>
                                                <a href="{{ route('barang.show', ['id' => $data->id_barang]) }}" class="btn btn-sm btn-primary btn-product">Detail</a>
                                                <a href="{{ route('barang.edit', ['id' => $data->id_barang]) }}" class="btn btn-sm btn-warning btn-product">Edit</a>
                                           
                                                <!-- DELETE BUTTON -->
                                                <button type="submit" class="btn btn-danger btn-sm btn-product" onclick="return confirm('Yakin Hapus Data?')">
                                                    Hapus 
                                                </button>
                                            </td>
                                            @csrf 
                                            @method('DELETE')
                                        </form>
                                        <!-- END FORM -->
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
                @if ($barang instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $barang->links() }}
                @endif
            </div>
        </div>
    </div>
</div> 
@endsection