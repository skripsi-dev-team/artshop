@extends('admin.app')

@section('title')
Detail Pembayaran 
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h3>Detail Pembayaran</h3>
                <table class="table">
                    <tr>
                        <td>No. Invoice</td>
                        <td>:</td>
                        <td>{{ $pembayaran->no_pembayaran }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ $pembayaran->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>Metode Pembayaran</td>
                        <td>:</td>
                        <td>{{ $pembayaran->metode_pembayaran }}</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>:</td>
                        <td>{{ $pembayaran->total_bayar }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h3>Detail Pesanan</h3>
                <table class="table">
                    <tr>
                        <td>Kode Pemesanan</td>
                        <td>:</td>
                        <td>{{ $pembayaran->pemesanan->kode_pemesanan }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ $pembayaran->pemesanan->tanggal }}</td>
                    </tr>
                    <tr>
                        <td>Nama Pemesan</td>
                        <td>:</td>
                        <td>{{ $pembayaran->pemesanan->customer->name }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ $pembayaran->pemesanan->customer->email }}</td>
                    </tr>
                    <tr>
                        <td>Status Pemesanan</td>
                        <td>:</td>
                        <td>{{ $pembayaran->pemesanan->getStatusText() }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>QTY</th>
                                <th>Harga</th>
                                <th>Total</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembayaran->pemesanan->details as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $data->barang->nama_barang }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>{{ $data->barang->harga }}</td>
                                    <td>{{ $data->barang->harga * $data->qty }}</td>
                                       
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection

