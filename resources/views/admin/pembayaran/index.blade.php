@extends('admin.app')

@section('title')
Data Pemesanan
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Invoice</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Pelanggan</th>
                                <th scope="col">Total</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $data)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $data->no_pembayaran }}</td>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>{{ $data->pemesanan->customer->name }}</td>
                                    <td>{{ $data->total_bayar }}</td>
                                    <td>
                                        <a href="{{ route('pembayaran.show', ['id' => $data->id_pembayaran]) }}" class="btn btn-sm btn-primary ">Detail</a>
                                        
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('scripts')

<script src="{{ asset('DataTables/datatables.min.js') }}"></script>

<script>

$('#datatable').DataTable();

</script>

@endpush

