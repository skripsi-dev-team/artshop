
@if(Session::get('success'))
<script>
    toastr.success('<?= Session::get('success') ?>', 'Success', {timeOut:0});
</script>
<?php Session::forget('success') ?>
@endif

@if(Session::get('warning'))
<script>
    toastr.warning('<?= Session::get('warning') ?>', 'Warning', {timeOut:5000});
</script>
<?php Session::forget('warning') ?>
@endif

@if(Session::get('danger'))
<script>
    toastr.error('<?= Session::get('danger') ?>', 'Error', {timeOut:5000});
</script>
<?php Session::forget('danger') ?>
@endif

