@extends('admin.app')

@section('title')
Diskon
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
        
                <a href="{{ route('diskon.create') }}" class="btn btn-outline-primary">Tambah Diskon</a>
                <br><br>
                <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Diskon</th>
                                <th scope="col">Tipe Diskon</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($discounts as $discount)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $discount->nama_diskon }}</td>
                                    <td>{{ $discount->getTypeDiskonText() }}</td>
                                    
                                        <!-- form (for delete) -->
                                        <form action="{{ route('diskon.destroy', ['id' => $discount->id_diskon] ) }}" id="delete-diskon" method="post">
                                            <!-- EDIT BUTTON -->
                                            <td>
                                                <a href="{{ route('diskon.edit', ['id' => $discount->id_diskon]) }}" class="btn btn-sm btn-warning">Edit</a>
                                           
                                            <!-- DELET BUTTON -->
                                           
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus Data?')">
                                                    Hapus 
                                                </button>
                                            </td>
                                            @csrf 
                                            @method('DELETE')
                                        </form>
                                        <!-- END FORM -->
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection