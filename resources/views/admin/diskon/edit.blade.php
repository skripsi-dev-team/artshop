@extends('admin.app')

@section('title')
Edit Diskon
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4>Form Diskon</h4>
                <hr>
                <form action="{{ route('diskon.update', $discount->id_diskon) }}" method="post">
                @csrf
                @method('PUT')
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="">Nama Diskon</label>
                            <input type="text" class="form-control" placeholder="Nama Diskon" name="nama_diskon" value="{{ $discount->nama_diskon }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Diskon</label><br>
                        <div class="form-inline">
                            <input type="radio" id="nominal" name="tipe_diskon" {{ $discount->tipe_diskon == 0 ? 'checked' : '' }} value="0" class="form-check-input">
                            <label for="nominal" class="form-check-label">Nominal</label>
                            &nbsp;
                            <input type="radio" id="persentase" name="tipe_diskon" {{ $discount->tipe_diskon == 1 ? 'checked' : '' }} value="1" class="form-check-input">
                            <label for="persentase" class="form-check-label">Persentase</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="">Tanggal Mulai</label>
                            <div class="input-group">
                                <input type="text" class="form-control dt-start" placeholder="Periode mulai diskon" name="tgl_mulai_diskon" value="{{ $discount->tgl_mulai_diskon }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Tanggal Berakhir</label>
                            <div class="input-group">
                                <input type="text" class="form-control dt-end" placeholder="Periode akhir diskon" name="tgl_akhir_diskon" value="{{ $discount->tgl_akhir_diskon }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="">Nominal</label>
                            <input type="number" name="nominal" placeholder="Nominal" class="form-control" value="{{ $discount->nominal }}" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="">Persentase</label>
                            <input type="number" name="persentase" placeholder="Persentase" class="form-control" value="{{ $discount->persentase }}" id="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="barang">Pilih Barang yang akan didiskon( Optional )</label>
                            <select name="barang_id[]" id="barang" multiple class="form-control select2">
                                
                                @foreach ($discount->items as $item)
                                    <option value="{{ $item->id_barang }}" selected>{{ $item->nama_barang }}</option>
                                @endforeach

                                @foreach ($items as $item)
                                    <option value="{{ $item->id_barang }}">{{ $item->nama_barang }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mr-auto">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Simpan">                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('select2/dist/js/select2.min.js') }}"></script>
<script>
(function($){
    $('.select2').select2({
        placeholder: 'Pilih barang yang akan didiskon'
    });
    $('.dt-start').datepicker({
        autoclose: true,
        format: 'yyyy-m-d',
        todayHighlight: true
    })
    .on('changeDate', function(e) {
        var end = $('.dt-end');
        
        end.datepicker({
            autoclose: true,
            format: 'yyyy-m-d',
            startDate: e.date
        });
        end.focus();
        
    });
})(jQuery)
</script>
@endpush