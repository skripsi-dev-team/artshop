@extends('admin.app')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card card-hover">
                <div class="box bg-primary text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-cart"></i> <?= $total_pesanan ?></h1>
                    <h6 class="text-white">Total Pesanan Masuk</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-hover">
                <div class="box bg-success text-center">
                    <h1 class="font-light text-white"><i class="fa fa-users"></i> <?= $total_pelanggan ?></h1>
                    <h6 class="text-white">Total Pelanggan</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-hover">
                <div class="box bg-secondary text-center">
                    <h1 class="font-light text-white"><i class="fa fa-box"></i> <?= $total_produk ?></h1>
                    <h6 class="text-white">Total Produk</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p>Selamat Datang {{ (Auth::user()->level) ? 'Owner' : 'Admin' }} :)</p>
                    
                </div>
            </div>
        </div>
    </div>
@endsection