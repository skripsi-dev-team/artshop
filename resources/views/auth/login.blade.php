@extends('admin.app')

@section('title')
Login Admin
@endsection

@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
    <div class="auth-box bg-dark border-top border-secondary">
        <div id="loginform">
            <div class="text-center p-t-20 p-b-20">
                <span class="db"><img src="{{ asset('images/logo-icon.png') }}" alt="logo"><b style="color:white"> Surya Artshop<b></span>
            </div>
            <!-- Form -->
            <form method="POST" action="{{ route('login') }}" class="form-horizontal m-t-20" id="loginform">
                @csrf
                <div class="row p-b-30">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                            </div>
                            <input id="email" type="email" placeholder="Email" class="form-control form-control-lg form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                            </div>
                            <input id="password" placeholder="Password" type="password" class="form-control form-control-lg form-control @error('password') is-invalid @enderror" name="password" required>
                        </div>
                    </div>
                </div>
                <div class="row border-top border-secondary">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="p-t-20">
                                <button type="submit" class="btn btn-success float-right">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    
    </div>
</div>


@endsection