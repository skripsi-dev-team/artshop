<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id_pengiriman');
            $table->string('no_resi', 100)->nullable();
            $table->string('provinsi', 25);
            $table->string('kota', 25);
            $table->date('tanggal_kirim')->nullable();
            $table->integer('ongkos_kirim');
            $table->tinyInteger('status_kirim')->unsigned();
            $table->string('jasa_kirim', 100);
            $table->string('paket_pengiriman', 50);
            $table->string('estimasi', 50);
            $table->text('alamat_kirim');
            $table->bigInteger('pemesanan_id')->unsigned();
            $table->foreign('pemesanan_id')->references('id_pemesanan')->on('pemesanan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman');
    }
}
