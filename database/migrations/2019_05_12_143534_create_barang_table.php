<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->bigIncrements('id_barang');
            $table->bigInteger('kategori_id')->unsigned();
            $table->string('sku', 100);
            $table->string('slug')->unique();
            $table->string('nama_barang');
            $table->integer('harga')->default(0);
            $table->float('harga_sale')->nullable();
            $table->binary('foto')->nullable();
            $table->float('berat');
            $table->float('panjang')->nullable()->default(0.0);
            $table->float('lebar')->nullable()->default(0.0);
            $table->float('tinggi')->nullable()->default(0.0);
            $table->text('deskripsi');
            $table->foreign('kategori_id')->references('id_kategori')->on('kategori_barang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
