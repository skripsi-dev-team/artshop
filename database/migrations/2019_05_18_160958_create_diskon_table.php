<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiskonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diskon', function (Blueprint $table) {
            $table->bigIncrements('id_diskon');
            $table->string('nama_diskon');
            $table->boolean('tipe_diskon');
            $table->date('tgl_mulai_diskon');
            $table->date('tgl_akhir_diskon');
            $table->integer('nominal')->unsigned()->nullable()->default(0);
            $table->integer('persentase')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diskon');
    }
}
