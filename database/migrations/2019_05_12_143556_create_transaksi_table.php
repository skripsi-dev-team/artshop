<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->bigIncrements('id_pemesanan');
            $table->bigInteger('pelanggan_id')->unsigned();
            $table->date('tanggal');
            $table->string('kode_pemesanan')->unique();
            $table->integer('total_nominal');
            $table->boolean('status_bayar');
            $table->tinyInteger('status_pemesanan');
            $table->foreign('pelanggan_id')->references('id_pelanggan')->on('pelanggan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
