<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranManualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran_manual', function (Blueprint $table) {
            $table->bigIncrements('id_pembayaran');
            $table->bigInteger('pemesanan_id')->unsigned();
            $table->dateTime('tanggal_transfer');
            $table->string('bank');
            $table->string('no_rekening_pelanggan');
            $table->integer('nominal');
            $table->binary('bukti_transfer');
            $table->foreign('pemesanan_id')->references('id_pemesanan')->on('pemesanan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_manual');
    }
}
