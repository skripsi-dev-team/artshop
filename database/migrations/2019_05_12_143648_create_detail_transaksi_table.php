<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pemesanan', function (Blueprint $table) {
            $table->bigIncrements('id_detail_pemesanan');
            $table->bigInteger('pemesanan_id')->unsigned();
            $table->bigInteger('barang_id')->unsigned();
            $table->integer('qty');
            $table->foreign('pemesanan_id')->references('id_pemesanan')->on('pemesanan');
            $table->foreign('barang_id')->references('id_barang')->on('barang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pemesanan');
    }
}
