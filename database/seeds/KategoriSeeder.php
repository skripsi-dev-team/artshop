<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\KategoriBarang::create([
            'nama_kategori'     => 'lukisan'
        ]);

        App\Model\KategoriBarang::create([
            'nama_kategori'     => 'patung'
        ]);

        App\Model\KategoriBarang::create([
            'nama_kategori'     => 'pahatan'
        ]);
    }
}
