<?php

use Illuminate\Database\Seeder;
use App\Model\Barang;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Barang::create([
            'kategori_id'  => 1,
            'sku' => '11223300',
            'slug'  => 'seeder-barang-1',
            'nama_barang'   => 'Seeder Barang 1',
            'harga' => '1500',
            'deskripsi' => 'Lorem ipsum dolor sit amet',
            'berat' => 1000,
            'panjang' => 10,
            'lebar' => 5,
            'tinggi' => 120
        ]);
        Barang::create([
            'kategori_id'  => 2,
            'sku' => '22334455',
            'slug'  => 'seeder-barang-2',
            'nama_barang'   => 'Seeder Barang 2',
            'harga' => '1700',
            'deskripsi' => 'Lorem ipsum dolor sit amet',
            'berat' => 1500,
            'panjang' => 15,
            'lebar' => 15,
            'tinggi' => 130
        ]);
        Barang::create([
            'kategori_id'  => 1,
            'sku' => '33445566',
            'slug'  => 'seeder-barang-3',
            'nama_barang'   => 'Seeder Barang 3',
            'harga' => '1750',
            'deskripsi' => 'Lorem ipsum dolor sit amet',
            'berat' => 500,
            'panjang' => 3,
            'lebar' => 20,
            'tinggi' => 130

        ]);
    }
}
