<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Clients\ClientController@index')->name('client.home');

Route::get('barang/', 'Clients\ClientController@showBarang')->name('client.show_barang');

Route::get('barang/{slug}', 'Clients\ClientController@detailBarang')->name('client.detail_barang');

Route::get('barang/kategori/{id}', 'Clients\ClientController@showKategori')->name('client.kategori');

Route::get('barang/search','Clients\ClientController@searchBarang')->name('barang.search');

Route::get('about', function(){
    return view('client.about');
});

Route::namespace('Clients')->group(function() {
    Route::get('register/', 'PelangganLoginController@showRegisterForm')->name('pelanggan.registerForm');
    Route::post('register', 'PelangganLoginController@register')->name('pelanggan.register');
    Route::get('login', 'PelangganLoginController@showLoginForm')->name('pelanggan.loginForm');
    Route::post('login', 'PelangganLoginController@login')->name('pelanggan.login');
    Route::post('logout', 'PelangganLoginController@logout')->name('pelanggan.logout');
    //auth

    Route::middleware('auth.client:pelanggan')->group(function(){
        Route::get('profile/{id}', 'PelangganController@index')->name('pelanggan.profile');
        Route::get('profile/edit/{id}', 'PelangganController@edit')->name('pelanggan.edit-profile');
        Route::put('profile/edit/{id}', 'PelangganController@update')->name('pelanggan.update');
        Route::get('profile/ganti_password/{id}', 'PelangganController@changePassword')->name('pelanggan.change_password');
        Route::put('profile/ganti_password/{id}', 'PelangganController@updatePassword')->name('pelanggan.update_password');
        
        Route::put('riwayat_pesanan/detail/{id}/terima', 'PelangganController@terimaBarang')->name('pelanggan.terima_barang');

        Route::get('barang-disukai', 'LikeController@index')->name('pelanggan.liked-items');

        Route::get('checkout', function() {
            return view('client.checkout.form', ['user' => Auth::guard('pelanggan')->user()]);
        })->name('client.checkout');
        
        Route::get('riwayat_pesanan', 'PelangganController@riwayatPesanan')->name('pelanggan.riwayat_pesanan');
        Route::get('riwayat_pesanan/detail/{id}', 'PelangganController@detailPesanan')->name('pelanggan.detail_pesanan');
        Route::get('riwayat_pesanan/detail', function(){
            return redirect()->back();
        });

        Route::put('riwayat_pesanan/cancel/{id}', 'ClientController@cancelOrder')->name('pelanggan.cancel_order');

        //komentar
        Route::post('barang/{slug}/comment', 'KomenController@create')->name('pelanggan.komen');
        
        Route::post('place-order', 'ClientController@placeOrder')->name('pelanggan.order');

        Route::get('thankyou/{id}', 'ClientController@thankyou')->name('pelanggan.order.thankyou');
    });
});

Route::prefix('doku')->group(function() {
    Route::post('identify', 'DokuController@identify');
    Route::post('notify', 'DokuController@notify');
    Route::post('redirect', 'DokuController@redirect');
});

Route::group(['prefix'  => 'api/cart', 'namespace' => 'Api'], function() {
    Route::get('all', 'CartController@cartList')->name('api.cart.all');
    Route::post('add', 'CartController@addToCart')->name('api.cart.add');
    Route::post('update', 'CartController@updateCart')->name('api.cart.update');
    Route::post('delete/{barang}', 'CartController@deleteCart')->name('api.cart.delete');

});

Route::group(['prefix' => 'api/', 'namespace' => 'Api'], function() {
    Route::middleware('auth.client:pelanggan')->group(function() {
        Route::get('likes', 'LikeController@index')->name('api.like.index');
        Route::post('likes/{item_id}', 'LikeController@create')->name('api.like.add');
        Route::post('likes/{item_id}/remove', 'LikeController@remove')->name('api.like.remove');
    });
    Route::get('cost', 'ShippingController@shippingCost')->name('api.shipping.cost');
});

Route::get('/test', function(){
	return view('default');
});


Route::get('admin/', function(){
    return redirect()->route('admin.home');
});

Route::prefix('admin')->group(function() {

    Route::middleware('auth')->group(function() {
        Route::resource('kategori', 'KategoriController');
        Route::get('dashboard', 'HomeController@index')->name('admin.home');
        Route::resource('barang', 'BarangController');
        Route::resource('user', 'UserController');
        Route::get('trash-barang', 'BarangController@showTrash')->name('barang.trash');
        Route::get('barang/{id}/restore', 'BarangController@restore')->name('barang.restore');
        Route::get('pelanggan', 'PelangganController@index')->name('pelanggan.index');
        Route::delete('pelanggan/{id}', 'PelangganController@destroy')->name('pelanggan.destroy');
        Route::resource('pemesanan', 'PemesananController');
        Route::resource('pembayaran', 'PaymentController');


        Route::get('laporan-penjualan', 'LaporanController@laporanPenjualan')->name('laporan.penjualan');

        Route::get('laporan-penjualan/print', 'LaporanController@cetakPenjualan')->name('print.penjualan');

        Route::get('laporan-stock', 'LaporanController@laporanStock')->name('laporan.stock');

        Route::get('laporan-stock/print', 'LaporanController@cetakStock')->name('print.stock');
    });
    
    Auth::routes();
});

