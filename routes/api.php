<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middelware' => 'auth:api'], function () {
    
    Route::delete('barang/{id}/delete-permanently', 'ApiController@deletePermanently')->name('api.barang.delete_permanent');
    Route::delete('pelanggan/{id}/delete', 'ApiController@deletePelanggan')->name('api.pelanggan.delete');
});

Route::namespace('Api')->group(function() {
    Route::get('provinces', 'ShippingController@fetchProvince')->name('api.shipping.province');
    Route::get('cities', 'ShippingController@fetchCity')->name('api.shipping.city');
});