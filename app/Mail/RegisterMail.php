<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    public $pelangganName;
    public $pelangganEmail;
    public $pelangganAlamat;
    public $pelangganNoTelp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pelangganName, $pelangganEmail, $pelangganAlamat, $pelangganNoTelp)
    {
        $this->pelangganName = $pelangganName;
        $this->pelangganEmail = $pelangganEmail;
        $this->pelangganAlamat = $pelangganAlamat;
        $this->pelangganNoTelp = $pelangganNoTelp;
        $this->subject('Registrasi Berhasil');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('client.mail.register_mail'); 
    }
}
