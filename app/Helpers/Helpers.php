<?php 

use Illuminate\Support\Facades\DB;
use App\Model\KategoriBarang;
use App\Model\Barang;
use Illuminate\Support\Facades\File;

function notifMsg($stat, $msg){
    \Session::put($stat, $msg);
}

function doRenameImage($image){
    $default_name = $image;
    $get_format_name = explode('.', $default_name);
    $file_gambar = rand(5, 15);
    $new_name = date('Y-m-d_His').'-'.$file_gambar.'.'. end($get_format_name);
    return $new_name;
}


function resizeImage($image){
    $img = Image::make('storage/gambar_produk/'.$image);
    $img->fit(300, 300, function ($constraint) {
        $constraint->upsize();
    });

    $img->save('storage/gambar_produk/small/'.$image);
    return $img;
}

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}

function showStock($barang) {
    $stock = Barang::find($barang)->stocks;
    if($stock){
        return $stock->jumlah;
    } else {
        return "unset";
    }
    
}


function generateBreadcrumb(){
    $url = $_SERVER['REQUEST_URI'];
    $crumbs = explode("/", $url);
    $str = '';
    foreach($crumbs as $crumb){
        $str .= "<li>". ucwords(str_replace('-', ' ', $crumb)) ."</li>";
       
    }

    $str = str_replace('?search=', ' / Search = ', $str);
    return $str;
}

function getCart() {
    if (Session::has('cart')) {
        return Session::get('cart');
    }

    return collect([]);
}

function getCartTotal() {
    $total = 0;

    foreach (getCart()->all() as $item) {
        $total += $item['price'] * $item['qty'];
    }

    return 'Rp. '.number_format($total);
}

function getTotalWeight() {
    $weight = 0;

    foreach (getCart()->all() as $item) {
        $weight += $item['weight'];
    }

    return $weight;
}

function clearCart() {
    Session::put('cart', collect([]));
}

function itemInCart($id) {
    $carts = getCart();
    if ($carts->count() > 0) {
        $cart = $carts->firstWhere('item_id', $id);
        if ($cart) {
            return true;
        }else {
            return false;
        }
    }
    return false;
}


function listCategory(){
    return KategoriBarang::all();
}

function listFooterCategory(){
    return KategoriBarang::limit(5)->get();
}

function generateOrderCode() {
    return 'ODR-'.now()->format('Ymd').\App\Model\Pemesanan::count();
}


function getWordsKey($amount, $order) {
    return sha1($amount.'.00' . env('MALLID', null) . env('SHAREDKEY', null) . $order);
}

function createBasket($order) {
    $basket = '';
    foreach ($order->details as $item) {
        $price = ($item->barang->harga_sale == 0) ? $item->barang->harga : $item->barang->harga_sale;
        $basket .= $item->barang->nama_barang;
        $basket .= ',';
        $basket .= $price.'.00';
        $basket .= ',';
        $basket .= $item->qty;
        $basket .= ',';
        $basket .= ($item->qty * $price).'.00';
        $basket .= ';';
    }

    $basket .= 'Ongkos kirim,';
    $basket .= $order->shipping->ongkos_kirim.'.00';
    $basket .= ',';
    $basket .= '1,';
    $basket .= $order->shipping->ongkos_kirim.'.00;';

    return $basket;    
}

function generateInvoiceNo($order_no) {
    $len = strlen($order_no);
    $start = $len - 4;

    return 'INV-'.now()->format('ymds').'-'.substr($order_no, $start);
} 

?>