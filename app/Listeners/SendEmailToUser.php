<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\OrderWasCreated;
use Mail;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        clearCart();
        $admin = \App\User::first();

        Mail::send('client.mail.order-received-client', ['order' => $event->order], function($m) use($event) {
            $m->from(env('MAIL_FROM_ADDRESS', 'sale@artshop.com'), 'Artshop');
            $m->to($event->order->customer->email, $event->order->customer->name)->subject('Artshop Order');
        });
        Mail::send('client.mail.order-received-admin', ['order' => $event->order], function($m) use($admin) {
            $m->from(env('MAIL_FROM_ADDRESS', 'sale@artshop.com'), 'Artshop');
            $m->to($admin->email, $admin->name)->subject('New Order!');
        });
    }
}
