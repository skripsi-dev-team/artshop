<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BarangDipersiapkanNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $pemesanan;
    private $customer;

    public function __construct($customer, $pemesanan)
    {
        $this->customer = $customer;
        $this->pemesanan = $pemesanan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pesanan Anda '.$this->pemesanan->kode_pemesanan.' Sedang Dipersiapkan')
                    ->greeting('Halo, '.$this->customer->name)
                    ->line('Pesanan anda '.$this->pemesanan->kode_pemesanan.' sedang dalam proses persiapan.')
                    ->line('Klik tombol dibawah untuk mengecek detail dan status pemesanan anda.')
                    ->action('Cek Status Pemesanan', url(env('APP_URL').'/riwayat_pesanan/detail/'.$this->pemesanan->kode_pemesanan))
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
