<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BarangDiterima extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $customer;
    private $pemesanan;

    public function __construct($customer, $pemesanan)
    {
        $this->customer = $customer;
        $this->pemesanan = $pemesanan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                        ->subject('Pesanan Anda '.$this->pemesanan->kode_pemesanan.' Telah Diterima')
                        ->greeting('Halo, '.$this->customer->name)
                        ->line('Pesanan anda '.$this->pemesanan->kode_pemesanan.' telah diterima.')
                        ->line('Untuk mengecek status pemesanan anda klik tombol dibawah')
                        ->action('Cek Status Pemesanan', url(env('APP_URL').'/riwayat_pesanan/detail/'.$this->pemesanan->kode_pemesanan))
                        ->line('Terima kasih telah melakukan pembelian pada toko kami. Kami tunggu pemesanan berikutnya.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
