<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PemesananBatalNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $customer;
    private $pemesanan;

    public function __construct($customer, $pemesanan)
    {
        $this->customer = $customer;
        $this->pemesanan = $pemesanan;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                        ->subject('Pesanan Anda '.$this->pemesanan->kode_pemesanan.' Dibatalkan')
                        ->greeting('Halo, '.$this->customer->name)
                        ->line('Pesanan anda '.$this->pemesanan->kode_pemesanan.' kami nyatakan batal.')
                        ->line('Dikarenakan anda tidak melakukan konfirmasi atau pembayaran dalam waktu yang sudah ditentukan. Apabila anda ingin melakukan pemesanan, silahkan kunjungi website kami dan lakukan transaksi pemesanan kembali.')
                        ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
