<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Model\Pemesanan;

class PaymentComplete
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $order;
    public function __construct(Pemesanan $order)
    {
        $this->order = $order;
    }

}
