<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePemesananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provinsi'  => 'required|string',
            'kota'  => 'required|string',
            'jasa_kirim'    => 'required|string',
            'paket_pengiriman'  => 'required|string'
        ];
    }
}
