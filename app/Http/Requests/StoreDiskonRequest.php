<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiskonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_diskon'   => 'required|string',
            'tipe_diskon'   => 'required|boolean',
            'tgl_mulai_diskon' => 'required',
            'tgl_akhir_diskon'  => 'required',
            'nominal'   => 'integer',
            'persentase'    => 'integer'
        ];
    }
}
