<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateBarang extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_barang' => 'required',
            'kategori_id' => 'required',
            'sku' => 'required',
            'slug' => 'required',
            'harga' => 'required|numeric',
            'berat' => 'required',
            'panjang'   => 'required',
            'lebar' => 'required',
            'tinggi'    => 'required',
            'deskripsi' => 'required'
        ];
    }
}
