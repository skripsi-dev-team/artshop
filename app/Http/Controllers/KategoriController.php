<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreKategori;
use App\Model\KategoriBarang;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = KategoriBarang::all();
        return view('admin.kategori.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKategori $request)
    {
        $validate = $request->validated();

        if($validate) {
            $input = $request->toArray();
            KategoriBarang::create($input);
            notifMsg('success', 'Berhasil menambah data kategori!');
            return redirect()->route('kategori.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kategori'] = KategoriBarang::find($id);
        if($data['kategori']){
            return view('admin.kategori.edit', $data);
        } else {
            notifMsg('danger', 'Data Kategori Tidak ditemukan!');
            return redirect()->route('kategori.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = KategoriBarang::find($id)->update($request->toArray());
        if($update) {
            notifMsg('success', 'Data Kategori Berhasil diedit');
            return redirect()->route('kategori.index');
        } else {
            notifMsg('danger', 'Data Kategori Gagal diedit!');
            return redirect()->route('kategori.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = KategoriBarang::destroy($id);
        
        if($delete) {
            notifMsg('warning', 'Hapus Kategori Sukses!');
            return redirect()->route('kategori.index');
        } else {
            notifMsg('danger', 'Hapus Kategori Gagal!');
            return redirect()->route('Kategori.index');
        }
    }
}
