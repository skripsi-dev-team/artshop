<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Model\Barang;
use App\Model\KategoriBarang;
use App\Http\Requests\StoreBarang;
use App\Http\Requests\StoreUpdateBarang;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['q'])){
            $data['barang'] = Barang::where('nama_barang', 'like', '%'.$_GET['q'].'%')->get();
        } else {
            $data['barang'] = Barang::paginate(8);
        }
        return view('admin.barang.index', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kategori'] = KategoriBarang::all();
        return view('admin.barang.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBarang $request)
    {
        $validate = $request->validated();

        if($validate){
            $file_name = doRenameImage($request->file('foto')->getClientOriginalName());
            $store_image = $request->file('foto')->storeAs('gambar_produk', $file_name);
            if($store_image){
                //resize gambar
                try{
                    resizeImage($file_name);
                } catch (Exception $e) {
                    dd($e->getMessage());
                }
            } else {
                notifMsg('danger', 'Gambar gagal diupload, gagal menambah data barang!');
                return redirect()->route('barang.index');
            }
            
            //input ke db
            $barang = Barang::create($request->all());

            
            $barang->foto = $file_name;
            $barang->save();
            //input ke table stok
            
            notifMsg('success', 'Berhasil menambah data barang!');
            return redirect()->route('barang.index');
        } else {
            notifMsg('danger', 'Gagal menambah data barang!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['barang'] = Barang::find($id);
        return view('admin.barang.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['barang'] = Barang::find($id);
        $data['kategori'] = KategoriBarang::all();
        return view('admin.barang.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateBarang $request, $id)
    {
        $validate = $request->validated();
        $barang = Barang::find($id);

        if($validate) {
            //update with gambar

            if($request->foto != null){
                $file_name = $barang->foto;

                $store_image = $request->foto->storeAs('gambar_produk', $file_name);
                
                if($store_image){
                    //resize gambar
                    try{
                        resizeImage($file_name);
                    } catch (Exception $e) {
                        dd($e->getMessage());
                    }
                } else {
                    notifMsg('danger', 'Gambar gagal diupload, gagal mengedit data barang!');
                    return redirect()->route('barang.index');
                }

                 $barang->update([
                     'kategori_id' => $request->kategori_id,
                     'sku' => $request->sku,
                     'slug' => $request->slug,
                     'nama_barang' => $request->nama_barang,
                     'harga' => $request->harga,
                     'foto' => $file_name,
                     'berat' => $request->berat,
                     'lebar' => $request->lebar,
                     'panjang' => $request->panjang,
                     'tinggi' => $request->tinggi,
                     'deskripsi' => $request->deskripsi,
                     'diskon' => $request->diskon,
                     'stock' => $request->stock
                     ]
                 );
                
            } else {
                //without image
                $barang->update($request->toArray());
            }

            if ($request->get('diskon_id') != 0) {
                $barang->setSalePriceBasedDiscount($barang->discount);
            }

            notifMsg('success', 'Berhasil mengedit data barang!');
            return redirect()->route('barang.index');
        } else {
            notifMsg('danger', 'Gagal mengedit data barang!');
            return redirect()->route('barang.index');
        }
        //dd($request->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Barang::destroy($id);
        if($delete){
            notifMsg('warning', 'Berhasil menghapus data barang');
            return redirect()->route('barang.index');
        } else {
            notifMsg('danger', 'Gagal menghapus data barang');
            return redirect()->route('barang.index');
        }
    }


    //Trash Barang
    public function showTrash(){
       
        $data['barang'] = Barang::onlyTrashed()->get();
        return view('admin.barang.trash', $data);
    }

    public function restore($id) {
        Barang::onlyTrashed()->where('id_barang', $id)->restore();
        notifMsg('success', 'Berhasil me-restore barang');
        return redirect()->back();
    }
}
