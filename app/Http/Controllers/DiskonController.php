<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Diskon;
use App\Model\Barang;
use App\Http\Requests\StoreDiskonRequest;

class DiskonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.diskon.index', ['discounts' => Diskon::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.diskon.create', ['items' => Barang::doesntHave('discount')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDiskonRequest $request)
    {
        $discount = Diskon::create($request->except('barang_id'));
        $items = $request->get('barang_id');

        if (count($items) > 0) {
            for ($i=0; $i < count($items); $i++) {
                $item = Barang::find($items[$i]);
                $item->setSalePriceBasedDiscount($discount);
            }
        }

        return redirect()->route('diskon.index')->with('success', 'Berhasil menambah diskon baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discount = Diskon::findOrFail($id);

        return view('admin.diskon.edit', ['discount' => $discount, 'items' => Barang::doesntHave('discount')->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDiskonRequest $request, $id)
    {
        $discount = Diskon::findOrFail($id);
        $discount->update($request->except('barang_id'));
        $items = $request->get('barang_id');

        $discount->dissociateAllItems();

        if ($items) {
            if (count($items) > 0) {
                for ($i=0; $i < count($items); $i++) {
                    $item = Barang::find($items[$i]);
                    $item->setSalePriceBasedDiscount($discount);
                }
            }
        }


        return redirect()->route('diskon.index')->with('success', 'Berhasil mengupdate diskon!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discount = Diskon::findOrFail($id);

        $discount->dissociateAllItems();

        $discount->delete();

        return redirect()->route('diskon.index')->with('success', 'Berhasil menghapus diskon!');
    }
}
