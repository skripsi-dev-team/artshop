<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pemesanan;
use App\Model\Barang;
use PDF;

class LaporanController extends Controller
{
    public function laporanPenjualan(Request $request){
        $request->sampai = date('Y-m-d', strtotime($request->sampai . ' +1 day'));
        if(isset($request->cari)){
            $data['laporan'] = Pemesanan::whereBetween('tanggal', [$request->dari, $request->sampai])
                                            ->whereNotIn('status_pemesanan', [0,5])->get();
        } else {
            $data['laporan'] = '';
        }
        return view('admin.laporan.laporan_penjualan', $data);
    }

    public function cetakPenjualan(){

        $data['laporan'] = Pemesanan::whereBetween('tanggal', [$_GET['dari'], $_GET['sampai']])
                                            ->whereNotIn('status_pemesanan', [0,5])->get();
        $pdf = PDF::loadView('admin.laporan.cetak_penjualan', $data);
        return $pdf->download('laporan_penjualan.pdf');
    }

    public function laporanStock(Request $request){
        $data['barang'] = Barang::all();
        return view('admin.laporan.laporan_stock', $data);
    }

    public function cetakStock(){
        $data['barang'] = Barang::all();
        $pdf = PDF::loadView('admin.laporan.cetak_stock', $data);
        return $pdf->download('laporan_stock.pdf');
    }
}
