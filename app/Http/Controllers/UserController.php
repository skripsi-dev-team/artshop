<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $s = $request->has('q') ? $request->get('q') : '';
        $users = new User;

        if (!empty($s)) {
            $users = $users->where('name', $s)->orWhere('email', $s);
        }

        return view('admin.user.index', ['users' => $users->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        if (!$user) {
            
            return redirect()->route('user.index')->with('error', 'Gagal menambah data user!');
            
        }
        return redirect()->route('user.index')->with('success', 'Berhasil menambah data user!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id)->update($request->all());

        if ($request->has('password')) {
            $user->updatePassword($request->get('password'));
        }

        if (!$user) {
            return redirect()->route('user.index')->with('success', 'Gagal mengupdate data user!');
        
        }
        return redirect()->route('user.index')->with('success', 'Berhasil mengupdate data user!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        if (!$user) {
            return redirect()->route('user.index')->with('error', 'Gagal menghapus data user!');
        }

        return redirect()->route('user.index')->with('error', 'Berhasil menghapus data user!');
    }
}
