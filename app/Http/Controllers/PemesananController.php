<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pemesanan;
use App\Http\Requests\StorePemesananRequest;
use App\Events\OrderWasCreated;
use App\Model\DetailPemesanan;
use App\Notifications\BarangDipersiapkanNotification;
use App\Notifications\BarangDiterima;
use App\Notifications\BarangPengirimanNotification;
use App\Notifications\PemesananBatalNotification;

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $pemesanan;
    private $detail_pemesanan;

    public function __construct(){
        $this->pemesanan = new Pemesanan();
        $this->detail_pemesanan = new DetailPemesanan();
    }

    public function index()
    {
        $data['pemesanan'] = $this->pemesanan->orderBy('tanggal', 'desc')->get();
        return view('admin.pemesanan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePemesanan $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['detail_pemesanan'] = $this->detail_pemesanan->where('pemesanan_id', $id)->get();
        $data['pemesanan'] = $this->pemesanan->find($id);
        return view('admin.pemesanan.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pemesanan = $this->pemesanan->find($id);

        $customer = $pemesanan->customer;

        switch($request->status_pemesanan){
            case 2:
                $customer->notify(new BarangDipersiapkanNotification($customer, $pemesanan));
            break;

            case 3:
                $customer->notify(new BarangPengirimanNotification($customer, $pemesanan));
            break;

            case 4:
                $customer->notify(new BarangDiterima($customer, $pemesanan));
            break;

            case 5:
                $customer->notify(new PemesananBatalNotification($customer, $pemesanan));
            break;
        }

        $pemesanan->update([
            'status_pemesanan' => $request->status_pemesanan
        ]);
        notifMsg('success', 'Berhasil merubah status pemesanan');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
