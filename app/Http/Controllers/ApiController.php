<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Barang;
use App\Model\Pelanggan;
use Session;

class ApiController extends Controller
{
    public function deletePermanently($id) {
        //get deleted data;
        $get_deleted = Barang::onlyTrashed()->where('id_barang', $id)->first();
        if($get_deleted){
            //do delete
            try{
                $get_deleted->stocks()->delete();
                $delete = $get_deleted->forceDelete();
            } catch (Illuminate\Database\QueryException $e) {
               
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Barang tidak bisa dihapus, karena sedang terhubung pada modul lain'
                ], 500);
            }
            if($delete){
                //remove file
                if($get_deleted->foto != null){
                    $image = 'storage/gambar_produk/'.$get_deleted->foto;
                    $image_small = 'storage/gambar_produk/small/'.$get_deleted->foto;
                    if(file_exists($image)){
                        unlink($image);
                    }
                    if(file_exists($image_small)) {
                        unlink($image_small);
                    }
                }
                notifMsg('warning', 'Berhasil menghapus data barang secara permanent');
                return response()->json([
                    'status'    => 'success',
                    'message'   => 'Barang berhasil dihapus permanen!'
                ], 200);
            } else {
                //gagal hapus = gambar tidak hilang
                notifMsg('danger', 'Gagal menghapus data barang secara permanent');
                return response()->json([
                    'status'    => 'success',
                    'message'   => 'Barang gagal dihapus permanen!'
                ], 200);
            }
        }

        
    }

    public function deletePelanggan($id){
        $pelanggan = new Pelanggan();

        try {
            $delete = $pelanggan->destroy($id);
        } catch(Illuminate\Database\QueryException $e ){
            return response()->json([
                'status'    => 'error',
                'message'   => 'Barang tidak bisa dihapus, karena sedang terhubung pada modul lain'
            ], 500);
        }
        if($delete) {
            notifMsg('warning', 'Hapus Pelanggan Sukses!');
            return response()->json([
                'status'    => 'success',
                'message'   => 'Pelanggan berhasil dihapus permanen!'
            ], 200);
        } 
    }
}
