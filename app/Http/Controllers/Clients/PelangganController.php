<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pelanggan;
use App\Model\Pemesanan;
use App\Model\DetailPemesanan;
use Hash;
use Auth;

class PelangganController extends Controller
{

    public function index($id){
        $check_auth = Pelanggan::findOrFail($id);
        if($check_auth->checkAuthPelanggan($id) == true){
            $data['profile'] = $check_auth;
        } else {
            return redirect()->back();
        }
        return view('client.pelanggan.index', $data);
    }

    public function edit($id){
        $check_auth = Pelanggan::findOrFail($id);
        if($check_auth->checkAuthPelanggan($id) == true){
            $data['profile'] = $check_auth;
        } else {
            return redirect()->back();
        }
        
        return view('client.pelanggan.edit', $data);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required|numeric',
        ]);
        
        Pelanggan::findOrFail($id)->update($request->toArray());

        return redirect()->route('pelanggan.profile', ['id' => $id])->with('success_update', 'Profile berhasil diperbarui');
        
    }

    public function changePassword($id){
        $check_auth = Pelanggan::findOrFail($id);
        if($check_auth->checkAuthPelanggan($id) == true){
            $data['profile'] =  Pelanggan::findOrFail($id);
        } else {
            return redirect()->back();
        }

        return view('client.pelanggan.change_password', $data);
    }

    public function updatePassword(Request $request, $id){
        $this->validate($request, [
            'old_password' => 'required|max:24',
            'password' => 'required|max:24|min:6|confirmed',
        ]);
        if (!(Hash::check($request->old_password, Auth::guard('pelanggan')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Password lama anda tidak cocok!");
        }
        if(strcmp($request->old_password, $request->password) == 0){
            return redirect()->back()->with("error","Password lama dan password baru tidak boleh sama!");
        }

        $pelanggan = Auth::guard('pelanggan')->user();
  
        $pelanggan->password = $request->password;
        $pelanggan->save();


        return redirect()->back()->with("success","Ubah Password berhasil!");
    }


    public function riwayatPesanan(){
        $id_pelanggan = Auth::guard('pelanggan')->user()->id_pelanggan;
        $data['pemesanan'] = Pemesanan::where('pelanggan_id', $id_pelanggan)->get();
        return view('client.pelanggan.riwayat_pesanan', $data);
    }

    public function detailPesanan($kode){
        $id_pelanggan = Auth::guard('pelanggan')->user()->id_pelanggan;
        $get_pemesanan = Pemesanan::where('kode_pemesanan', $kode)->where('pelanggan_id', $id_pelanggan)->first();
        
        if($get_pemesanan === null){
            return redirect()->back()->with('fail', 'Data Pemesanan Tidak Ada!');
        }
    
        $data['detail_pemesanan'] = DetailPemesanan::where('pemesanan_id', $get_pemesanan->id_pemesanan)->get();
        $data['pemesanan'] = $get_pemesanan;
        return view('client.pelanggan.detail_pesanan', $data);
    }
    
    public function terimaBarang($id, Request $request){
        $pemesanan = Pemesanan::find($id);

        $pemesanan->update([
            'status_pemesanan' => $request->status_pemesanan
        ]);

        return redirect()->back()->with('success', 'Berhasil menerima barang');
    }

}
