<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Model\Pelanggan;


class PelangganLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:pelanggan')->except('logout');
    }

    public function index(){
        return view('client.home');
    }

    public function showLoginForm()
    {
        return view('client.auth.login');
    }

    public function showRegisterForm()
    {
        return view('client.auth.register');
    }

    protected function guard()
    {
        return Auth::guard('pelanggan');
    }


    public function register(Request $request)
    {
        $pelanggan = new Pelanggan;
        $request->validate([
            'name'              => 'required|string',
            'alamat'            => 'required',
            'no_telp'           => 'required',
            'email'             => 'required|string|email|unique:pelanggan',
            'password'          => 'required|string|min:6|confirmed'
        ]);

        $register = Pelanggan::create($request->all());
        $pelanggan->sendRegisterMail($register->id_pelanggan);
        return redirect()->back()->with('ok', 'Register Berhasil! Silahkan Login');
    }
}
