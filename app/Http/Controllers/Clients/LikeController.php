<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LikeController extends Controller
{
    public function index() {
        $user = Auth::guard('pelanggan')->user();

        return view('client.like.index', ['items' => $user->likes()->orderBy('nama_barang')->get()]);
    }
}
