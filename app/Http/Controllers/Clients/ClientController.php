<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Barang;
use App\Model\KategoriBarang;
use App\Model\Pemesanan;
use Auth;
use App\Events\OrderWasCreated;
use App\Http\Requests\StorePemesananRequest;

class ClientController extends Controller
{
    
    public function index(){       
        $data['barang'] = Barang::limit(3)->get();
        return view('client.home', $data);
    }

    public function showBarang(){
        $data['barang'] = Barang::paginate(9);

        return view('client.barang.index', $data);
    }

    public function detailBarang($slug, Request $request){
        //slug nya bikin error parameter,
        $data['authenticated'] = Auth::guard('pelanggan')->check();
        $data['user'] = Auth::guard('pelanggan')->user();
        $search = $request->has('search') ? $request->get('search') : false;

        if ($search) {
            //kalo di url ada kata search akan nampilkan search barang
            $data['barang'] = Barang::where('nama_barang', 'like', '%'.$search.'%')->get();
            return view('client.barang.index', $data);
        } else {
            //jika tidak, menampilkan detail barang
            $data['barang'] = Barang::where('slug', $slug)->first();
            
            if($data['barang'] == null){
                return redirect('barang')->with('barang_error', 'Barang tidak ditemukan');
            }
            $data['similar'] = Barang::where('kategori_id', $data['barang']->kategori_id)
                                ->where('id_barang', '!=', $data['barang']->id_barang)
                                ->paginate(4);
            
            
            return view('client.barang.detail', $data);
        }
       
    }
    
    public function showKategori($id) {
        $data['barang'] = Barang::where('kategori_id', $id)->paginate(9);
        if(count($data['barang']) != 0){
            $nama_kategori = KategoriBarang::find($id);
            $data['category'] = $nama_kategori->nama_kategori;
        } else {
            $data['category'] =  "Data kategori tidak ada";
        }
        
        
        $data['authenticated'] = Auth::guard('pelanggan')->check();
        $data['user'] = Auth::guard('pelanggan')->user();

        return view('client.barang.index', $data);
    }

    public function placeOrder(StorePemesananRequest $request) {
        $order = Pemesanan::create([
            'kode_pemesanan' => generateOrderCode(),
            'pelanggan_id' => $request->get('pelanggan_id'),
            'tanggal'   => now()->format('Y-m-d'),
            'total_nominal' => $request->get('total_nominal'),
            'status_bayar' => 0,
            'status_pemesanan'  => 0
        ]);

        $details = getCart()->all(['item_id', 'qty']);

        foreach ($details as $item) {
            $order->details()->create([
                'barang_id' => $item['item_id'],
                'qty'   => $item['qty']
            ]);
        }

        $order->shipping()->create([
            'provinsi'  => $request->get('provinsi'),
            'kota'  => $request->get('kota'),
            'status_kirim' => 0,
            'ongkos_kirim'  => $request->get('ongkos_kirim'),
            'alamat_kirim'  => $request->get('alamat_kirim'),
            'jasa_kirim' => $request->get('jasa_kirim'),
            'estimasi'  => $request->get('estimasi'),
            'paket_pengiriman' => $request->get('paket_pengiriman'),
            'kode_pos'  => $request->get('kode_pos')
        ]);

        event(new OrderWasCreated($order));

        return redirect()->route('pelanggan.order.thankyou', ['id' => $order->id_pemesanan]);
    }

    public function thankyou($id) {
        $order = Pemesanan::find($id);
        return view('client.checkout.thankyou', ['order' => $order]);
    }


    public function cancelOrder($id){
        $pemesanan = new Pemesanan();
        
        if($pemesanan->find($id)->status_bayar == 0){
            $pemesanan->find($id)->update([
                'status_pemesanan' => 5
            ]);
            return redirect()->back()->with('ok', 'Pesanan berhasil dibatalkan');
        } else {
            return redirect()->back()->with('fail', 'Pesanan gagal dibatalkan karena sudah terbayar!');
        }
    }
}
