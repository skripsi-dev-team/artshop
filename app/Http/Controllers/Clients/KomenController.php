<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Komen;
use App\Model\Barang;
use App\Model\Pelanggan;
use Auth;

class KomenController extends Controller
{
    //
    public function create(Request $request, $slug){
        $this->validate($request, ['komen' => 'required']);
        $barang = new Barang();

        $komen = Komen::create([
            'pelanggan_id' => Auth::guard('pelanggan')->user()->id_pelanggan,
            'komen' => $request->komen,
            'tanggal' => date('Y-m-d H:i:s')
        ]);

        $barang->find($barang->slugToId($slug))->comments()->attach($komen->id_komen);

        return redirect()->back()->with('ok', 'Berhasil menambahkan komentar');
    }
}
