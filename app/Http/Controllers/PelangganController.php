<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pelanggan;

class PelangganController extends Controller
{
    //pelanggan controller for backend;
    public function index(){
        $data['pelanggan'] = Pelanggan::all();
        return view('admin.pelanggan.index', $data);
    }

   
}
