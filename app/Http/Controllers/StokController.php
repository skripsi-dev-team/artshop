<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\StokBarang;
use App\Model\Barang;
use Illuminate\Support\Facades\DB;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['q'])){
            $data['stok'] = DB::table('barang')->where('nama_barang', 'like', '%'.$_GET['q'].'%')->get();
        } else {
            $data['stok'] = Barang::paginate(10);
        }
        $data['menu'] = 3;
        $data['no'] = 1;
        return view('admin.stok.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = 3;
        $data['barang'] = Barang::find($id);
        return view('admin.stok.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['jumlah' => 'required|min:0']);
        Barang::find($id)->updateStock($request->jumlah);
        notifMsg("success", "Berhasil meng-update stok barang");
        return redirect()->route('stok.index');

    }

    public function search(Request $request){
        dd($request->q);
    }
}

