<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pemesanan;
use App\Events\PaymentComplete;

class DokuController extends Controller
{
    public function identify(Request $request) {
        $orderId = $request->get('TRANSIDMERCHANT');

        $order = Pemesanan::where('kode_pemesanan', $orderId)->first();
        if ($order) {
            return 'CONTINUE';
        }

        return 'STOP';
    }
    public function notify(Request $request) {

        \Log::info("Payment notify begin...");
        if ($request->RESULTMSG == 'SUCCESS') {
            
            \Log::info("Result Message Success");
            
            $order = Pemesanan::where('kode_pemesanan', $request->TRANSIDMERCHANT)->first();
            
            if(!$order) {
                \Log::info("Order cannot be found!");
                return "STOP";
            }
            

            if ($order->status_bayar == 0) {
                $order->status_bayar = 1;
                $order->status_pemesanan = 2;
                $order->save();

                $order->payment()->create([
                    'tanggal' => now()->format('Y-m-d'),
                    'metode_pembayaran' => 'doku',
                    'no_pembayaran' => generateInvoiceNo($order->kode_pemesanan),
                    'total_bayar' => (int)$request->AMOUNT,
                ]);

                event(new PaymentComplete($order));
                \Log::info("Looks good should be print CONTINUE");
                return "CONTINUE";
            }

            return "STOP";

        }

        return "STOP";

    }

    public function redirect(Request $request) {
        $orderId = $request->get('TRANSIDMERCHANT');
        $order = Pemesanan::where('kode_pemesanan', $orderId)->first();

        if ($request->get('STATUSCODE') == '0000') {

            if (!$order) {
                return redirect()->route('pelanggan.riwayat_pesanan');
            }
            return redirect()->route('pelanggan.order.thankyou', ['id' => $order->id_pemesanan])->with('success', 'Terimakasih telah melakukan pembayaran');
        }

        return redirect()->route('pelanggan.riwayat_pesanan');
    }
}
