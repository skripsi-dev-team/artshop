<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Barang;
use Session;

class CartController extends Controller
{

    public function cartList() {
        return response()->json([
            'data'  => getCart()->all()
        ], 200);
    }

    public function addToCart(Request $request) {
        $items = getCart()->all();
        $id = $request->get('barang_id');
        $item = Barang::find($id);
        $item_price = ($item->diskon == 0 || $item->diskon == null) ? $item->harga : $item->getDiscountPrice();

        if (!$item) {
            return response()->json([
                'status'    => 'error',
                'message'   => 'Barang tidak ditemukan!'
            ], 404);
        }

        if (count($items) > 0) {

            if (itemInCart($id)) {
                for ($i = 0; $i < count($items); $i++) {
                    if ($items[$i]['item_id'] == $id) {
                        $items[$i]['qty'] += 1;
                        break;
                    }
                }
            }else {
                $items[] = [
                    'id'    => now()->timestamp,
                    'item_id' => $item->id_barang,
                    'item_name' => $item->nama_barang,
                    'price' => $item_price,
                    'formatted_price' => $item->formattedPrice($item_price),
                    'weight'    => $item->berat,
                    'qty'   => 1,
                ];
            }

        }else {
            $items[] = [
                'id'    => now()->timestamp,
                'item_id' => $item->id_barang,
                'item_name' => $item->nama_barang,
                'price' => $item_price,
                'formatted_price' => $item->formattedPrice($item_price),
                'weight'    => $item->berat,
                'qty'   => 1,
            ];    
        }

        Session::put('cart', collect($items));

        return response()->json([
            'status'    => 'ok',
            'message'   => 'Barang sudah ditambah ke keranjang',
            'cart'      => $items
        ], 200);
        
    }

    public function updateCart(Request $request) {
        $qtys = $request->get('qtys');
        $items = getCart()->all();

        for ($i=0; $i < count($items); $i++) { 
            $items[$i]['qty'] = $qtys[$i]; 
        }

        Session::put('cart', collect($items));

        return response()->json([
            'status'    => 'ok',
            'message'   => 'Cart terupdate!'
        ], 200);
    }

    public function deleteCart(Barang $barang, Request $request) {
        $items = getCart()->all();
        for ($i=0; $i < count($items); $i++) { 
            if ($items[$i]['item_id'] == $barang->id_barang) {
                array_splice($items, $i, 1);
                break;
            }
        }

        Session::put('cart', collect($items));

        return response()->json([
            'status'    => 'ok',
            'message'   => 'Barang sudah dihapus dari keranjang'
        ], 201);
    }
}
