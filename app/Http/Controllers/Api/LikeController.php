<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Barang;
use Auth;

class LikeController extends Controller
{

    public function index() {
        $user = Auth::guard('pelanggan')->user();

        return response()->json([
            'data'  => $user->likes
        ], 200); 
    }

    public function create($item_id) {
        $user = Auth::guard('pelanggan')->user();
        $user->likes()->attach($item_id);

        return response()->json([
            'status'    => 'ok',
            'message'   => 'Barang berhasil ditambahkan dalam daftar suka'
        ], 201);
    }

    public function remove($item_id, Request $request) {
        $user = Auth::guard('pelanggan')->user();
        $user->likes()->detach($item_id);

        session()->flash('success', 'Barang berhasil dihapus dari daftar suka');

        return response()->json([
            'status'    => 'ok',
            'message'   => 'Barang berhasil dihapus dari daftar suka'
        ], 200);
    }
}
