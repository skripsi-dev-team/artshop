<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShippingController extends Controller
{
    public function fetchProvince() {
        $request = curl_init('https://api.rajaongkir.com/starter/province');
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            "content-type: application/x-www-form-urlencoded",
            "key: ".env('RAJAONGKIR_KEY', '')
        )); 

        $response = curl_exec($request);        
        curl_close($request);

        return response()->json(json_decode($response), 200);
    }

    public function fetchCity(Request $request) {
        $url = 'https://api.rajaongkir.com/starter/city';

        if ($request->has('province')) {
            $url = 'https://api.rajaongkir.com/starter/city?province='.$request->get('province');
        }

        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            "content-type: application/x-www-form-urlencoded",
            "key: ".env('RAJAONGKIR_KEY', '')
        ));

        $response = curl_exec($request);
        curl_close($request);

        return response()->json(json_decode($response), 200);
    }

    public function shippingCost(Request $request) {
        
        $total_weight = getTotalWeight();

        $req = curl_init('https://api.rajaongkir.com/starter/cost');
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($req, CURLOPT_POSTFIELDS, 'origin='.env('SHIPPING_ORIGIN', '114').'&destination='.$request->get('destination').'&weight='.$total_weight.'&courier='.$request->get('courier'));
        curl_setopt($req, CURLOPT_HTTPHEADER, array(
            "content-type: application/x-www-form-urlencoded",
            "key: ".env('RAJAONGKIR_KEY', '')
        ));

        $response = curl_exec($req);
        curl_close($req);

        return response()->json(json_decode($response), 200);
    }
}
