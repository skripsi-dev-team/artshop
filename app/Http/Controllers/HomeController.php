<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pemesanan;
use App\Model\Pelanggan;
use App\Model\Barang;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['menu'] = 0;
        $data['total_pesanan'] = Pemesanan::count('id_pemesanan');
        $data['total_pelanggan'] = Pelanggan::count('id_pelanggan');
        $data['total_produk'] = Barang::count('id_barang');
        return view('admin.dashboard', $data);
    }
}
