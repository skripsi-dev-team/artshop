<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';

    protected $fillable = [
        'pemesanan_id',
        'tanggal',
        'metode_pembayaran',
        'no_pembayaran',
        'total_bayar'
    ];

    protected $primaryKey = 'id_pembayaran';

    public function detailPembayaran() {
        return $this->hasMany(DetailPembayaran::class, 'pembayaran_id');
    }

    public function pemesanan() {
        return $this->belongsTo(Pemesanan::class, 'pemesanan_id');
    }
}
