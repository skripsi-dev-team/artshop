<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use App\Mail\RegisterMail;

class Pelanggan extends Authenticatable
{
 
    use Notifiable;
    protected $table = 'pelanggan';

    protected $fillable = [
        'name',
        'email',
        'password',
        'alamat',
        'no_telp'
    ];

    protected $primaryKey = 'id_pelanggan';

    protected $hidden = ['password'];

    public function setPasswordAttribute($val) {
        return $this->attributes['password'] = bcrypt($val);
    }

    public function transaksi() {
        return $this->hasMany(Transaksi::class, 'pelanggan_id');
    }

    public function likes() {
        return $this->belongsToMany(Barang::class, 'likes', 'pelanggan_id', 'barang_id');
    }

    public function checkAuthPelanggan($id){
        if($id == Auth::guard('pelanggan')->user()->id_pelanggan){
            return true;
        } else {
            return false;
        }
    }

    public function sendRegisterMail($id){
        $pelanggan = Pelanggan::find($id);
        $regMail = new RegisterMail($pelanggan->name, $pelanggan->email, $pelanggan->alamat, $pelanggan->no_telp);
        Mail::to($pelanggan->email)->send($regMail);
    }
}
