<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'pemesanan';

    protected $fillable = [
        'pelanggan_id',
        'tanggal',
        'kode_pemesanan',
        'total_nominal',
        'status_bayar',
        'status_pemesanan'
    ];

    protected $primaryKey = 'id_pemesanan';

    private $statusText = [
        '0' => 'Menunggu Pembayaran',
        '1' => 'Pembayaran Sukses',
        '2' => 'Barang sedang dipersiapkan',
        '3' => 'Barang dalam pengiriman',
        '4' => 'Barang diterima',
        '5' => 'Pesanan dibatalkan'
    ];

    public function customer() {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function details() {
        return $this->hasMany(DetailPemesanan::class, 'pemesanan_id');
    }

    public function payment() {
        return $this->hasOne(Pembayaran::class, 'pemesanan_id');
    }

    public function shipping() {
        return $this->hasOne(Pengiriman::class, 'pemesanan_id');
    }

    public function getStatusText() {
        
        return $this->statusText[$this->status_pemesanan];
    }

    public function statusBadge(){
        if($this->status_pemesanan == 0) {
            $color = "warning";
        } else if ($this->status_pemesanan == 1){
            $color = "info";
        } else if ($this->status_pemesanan == 2 ) {
            $color = "primary";
        } else if($this->status_pemesanan == 3) {
            $color = "secondary";
        } else if ($this->status_pemesanan == 4) {
            $color = "success";
        } else if ($this->status_pemesanan == 5) {
            $color = "danger";
        }

        return "<span class='badge badge-".$color."'>".$this->statusText[$this->status_pemesanan]."</span>";
    }
    public function statusText($arr){
        return $this->statusText[$arr];
    }

    public function getFormattedTotal() {
        return 'Rp. '.number_format($this->total_nominal);
    }

}
