<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    protected $fillable = [
        'nama_diskon',
        'tipe_diskon',
        'tgl_mulai_diskon',
        'tgl_akhir_diskon',
        'nominal',
        'persentase'
    ];

    protected $primaryKey = 'id_diskon';

    protected $table = 'diskon';

    public function items() {
        return $this->hasMany(Barang::class, 'diskon_id');
    }

    public function dissociateAllItems() {
        if ($this->items) {
            foreach ($this->items as $item) {
                $item->diskon_id = 0;
                $item->harga_sale = 0;
                $item->save();
            }
        }
    }

    public function getTypeDiskonText() {
        $tipe = $this->attributes['tipe_diskon'];
        
        if ($tipe === 0) {
            return 'nominal';
        }

        return 'persentase';
    }
}
