<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = 'pengiriman';

    protected $fillable = [
        'pemesanan_id',
        'no_resi',
        'provinsi',
        'kota',
        'tanggal_kirim',
        'ongkos_kirim',
        'status_kirim',
        'jasa_kirim',
        'paket_pengiriman',
        'estimasi',
        'alamat_kirim',
    ];

    protected $primaryKey = 'id_pengiriman';

    public function getShippingPrice() {
        return 'Rp. '.number_format($this->ongkos_kirim);
    }
}
