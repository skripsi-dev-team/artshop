<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriBarang extends Model
{
    protected $fillable = [
        'nama_kategori'
    ];

    protected $primaryKey = 'id_kategori';

    protected $table = 'kategori_barang';
}
