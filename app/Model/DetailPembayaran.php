<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPembayaran extends Model
{
    protected $table = 'detail_pembayaran';

    protected $fillable = [
        'pembayaran_id',
        'no_rek_pelanggan',
        'bank',
        'nominal',
        'bukti_transfer'
    ];

    protected $primaryKey = 'id_pembayaran';

    public function pembayaran() {
        return $this->belongsTo(Pembayaran::class, 'pembayaran_id');
    }
}
