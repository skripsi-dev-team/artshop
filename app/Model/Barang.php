<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Pelanggan;
use Storage;

class Barang extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id_barang',
        'kategori_id',
        'sku',
        'slug',
        'nama_barang',
        'harga_sale',
        'harga',
        'foto',
        'berat',
        'panjang',
        'lebar',
        'tinggi',
        'deskripsi',
        'diskon',
        'stock'
    ];

    protected $primaryKey = 'id_barang';

    protected $table = 'barang';

    public function category() {
        return $this->belongsTo(KategoriBarang::class, 'kategori_id');
    }

    public function checkDiscount(){
        if($this->diskon == 0 || $this->diskon == null){
            echo number_format($this->harga);
        } else {
            $potongan = ($this->diskon / 100) * $this->harga;
            $diskon = $this->harga - $potongan;
            echo "<b>Diskon ". $this->diskon ."%</b>";
            echo "<br><strike>".number_format($this->harga)."</strike>" . "<br><b>".number_format($diskon)."</b>";
        }
    }

    public function getDiscountPrice(){
        $potongan = ($this->diskon / 100) * $this->harga;
        $diskon = $this->harga - $potongan;
        return $diskon;
    }
    


    public function getImageUrl() {
        return Storage::url('gambar_produk/small/'.$this->foto);
    }


    public function comments(){
        return $this->belongsToMany('App\Model\Komen', 'barang_komen', 'barang_id', 'komen_id')->withTimestamps();
    }

    public function slugToId($slug){
        $barang = Barang::where('slug', $slug)->first();
        return $barang->id_barang;
    }

    public function formattedPrice($price) {
        return 'Rp '.number_format($price);
    }

    public function getRealPrice() {
        if ($this->diskon == 0) {
            return $this->formattedPrice($this->harga);
        }

        return $this->formattedPrice($this->getDiscountPrice());
    }

    public function getTotalQty($qty) {
        if ($this->diskon == 0) {
            return $this->harga * $qty;
        }

        return $this->getDiscountPrice() * $qty;
    }

    public function inLikeList($user) {
        $item = $user->likes()->where('id_barang', $this->id_barang)->count();

        if ($item > 0) {
            return true;
        }

        return false;
    }

    public function likeds() {
        return $this->belongsToMany(Pelanggan::class, 'likes', 'barang_id', 'pelanggan_id');
    }
}
