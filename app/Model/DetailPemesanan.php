<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailPemesanan extends Model
{
    protected $table = 'detail_pemesanan';

    protected $fillable = [
        'pemesanan_id',
        'barang_id',
        'qty'
    ];

    protected $primaryKey = 'id_detail_pemesanan';

    public function barang() {
        return $this->belongsTo(Barang::class, 'barang_id');
    }

    public function pemesanan() {
        return $this->belongsTo(Pemesanan::class, 'pemesanan_id');
    }
    
    public function getTotal() {
        if ($this->barang->diskon == 0) {
            return $this->barang->getDiscountPrice() * $this->qty;
        }
        
        return $this->barang->harga * $this->qty;
    }
    
    public function getTotalFormat() {
        return 'Rp. ' . number_format($this->getTotal());
    }
}
