<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Komen extends Model
{
    protected $table = 'komen';

    protected $fillable = [
        'pelanggan_id', 'tanggal', 'komen'
    ];

    protected $primaryKey = 'id_komen';


    public function barangs(){
        return $this->belongsToMany('App\Model\Barang', 'barang_komen', 'barang_id', 'komen_id')->withTimestamps();
    }

    public function pelanggan(){
        return $this->belongsTo('App\Model\Pelanggan', 'pelanggan_id');
    }
}
