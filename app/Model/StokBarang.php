<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StokBarang extends Model
{
    protected $fillable = [
        'barang_id',
        'jumlah'
    ];

    protected $table = 'stok_barang';

    
}
