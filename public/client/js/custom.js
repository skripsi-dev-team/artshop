(function($) {

const artshop = {
    notify: {
        config: {
            type: 'success',
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },
            placement: {
                from: 'top',
                align: 'right'
            },
            z_index: 99999999
        }
    },
    cart: '',
    formatNumber: function(number, currency) {
        return currency + number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    },
    addToCart: function() {

        var self = this;
        $('.add-to-cart').on('click', function(e) {
            e.preventDefault();
            var btn = $(this);

            btn.val("Adding to cart....");

            $.post($(this).attr('data-url'), {
                barang_id: $(this).attr('data-id')
            }, function(response) {
                btn.val('Add to cart');
                if (response.status == 'ok') {
                    $('#cartModal').modal('show');
                }
            })
            .error(function(error) {
                btn.val('Add to cart');
                console.log(error.responseText);
                
            })
        });
    },
    fetchCart: function() {
        var self = this;
        $.get('/api/cart/all', function(response) {
            var html = '';

            self.cart = response.data;

            if (self.cart.length === 0) {
                html += '<tr>';
                html += '<td colspan="3"><center>Cart masih kosong</center></td>';
                html += '</td>';
                $('#cartTotal').html(self.formatNumber(0, 'Rp. '));
            }else{
                self.cart.forEach(function(item) {
                    html += '<tr data-id="'+item.item_id+'" >';
                    html += '<td><a href="#" class="rm-cart"><i class="fa fa-times"></a></td>';
                    html += '<td>' + item.item_name + '</td>';
                    html += '<td>' + item.formatted_price + '</td>';
                    html += '<td><input type="number" value="'+item.qty+'" class="cart-qty"></td>';
                    html += '</tr>';
                });
                
                var total = self.cart.map(function(item) {
                    return item.price * item.qty;
                }).reduce(function(total, current) {
                    return total + current;
                });
                $('#cartTotal').html(self.formatNumber(total, 'Rp. '));
            }

            $('tbody.cart-items').html(html);
        });
    },
    onCartModalShow: function() {
        var self = this;
        $('#cartModal').on('show.bs.modal', function(e) {
            self.fetchCart();
        })
    },
    removeCart: function() {
        var self = this;
        $(document).on('click', '.rm-cart', function(e) {
            e.preventDefault();
            var parent = $(this).parent().parent();

            $.post('/api/cart/delete/'+parent.attr('data-id'), function(response) {
                if (response.status == 'ok') {
                    parent.remove();
                }
                $.notify({
                    title: '<strong>Notification</strong>',
                    message: response.message,
                }, self.notify.config);
                self.fetchCart();    
            })
        })
    },
    updateCart: function() {
        var self = this;

        $('.update-cart').on('click', function(e) {
            e.preventDefault();
            $(this).val('Updating....');

            $.post('/api/cart/update', {
                qtys: self.cart.map(function(item) {
                    return item.qty;
                })
            }, function(response) {
                $(this).val('Update cart');
                $.notify({
                    title: '<strong>Notification</strong>',
                    message: response.message,
                }, self.notify.config);
                self.fetchCart(); 
            });
        })
    },
    onChangeQty: function() {
        var self = this;

        $(document).on('change', '.cart-qty', function(e) {
            var qty = $(this).val();

            if (qty >= 1) {
                var cartid = $(this).parent().parent().attr('data-id');
                var cart = self.cart.find(function(item) {
                    return item.item_id == cartid
                });
    
                cart.qty = qty;
            }
        })
    },
    likeItem: function() {
        var self = this;
        $('#like').not('.liked').on('click', function(e) {
            var a = $(this);
            $.post(a.attr('data-url'), function(response) {
                if (response.status == 'ok') {
                    a.removeClass('fa-heart-o');
                    a.addClass('fa-heart liked');
                }
            })
            .fail(function(error) {
                self.notify.config.type = 'danger';
                $.notify({
                    title: '<strong>Error</strong>',
                    message: error.responseText
                });
            })
        });
    }
}

artshop.addToCart();
artshop.onCartModalShow();
artshop.removeCart();
artshop.updateCart();
artshop.onChangeQty();
artshop.likeItem();

})(jQuery)